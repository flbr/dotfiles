=============================
|______ CRESCENT LOOM ______|
|_____ by Olive Perry  _____|
|_______ June 21 2021 _______|
|_________ v0.7.16 _________|
=============================

crescentloom.com
olive@wick.works

Crescent Loom is a game about creating life. Knit bones, stitch muscles, and weave neurons into a biologically-realistic simple creature.
It is a major work-in-progress; set your expectations accordingly.

If you have comments or share your enthusiasm for HOW FREAKING COOL it is to make artificial life, here are a few avenues for expressing that:

> wick.itch.io/crescent-loom
> twitter.com/crescentloom
> reddit.com/r/crescentloom

The best way to follow development of this is by signing up to my newsletter at:
https://tinyletter.com/wick

===========================
 Key validation
===========================

The default public creature server is set up so anybody can save creatures to it, but nobody can delete or overwrite existing creatures. It's the best way I could figure out how to give everyone access but not let people mess each other up.

There is now the option to make private pools using a key that comes with purchasing the game. Anyone with the server name you choose can open creatures in the connectome explorer, but you must have the server name + key to save / delete creatures in that pool.

To find your key and validate it, visit crescentloom.com/validate


===========================
 Hidden keyboard controls
===========================

End				unlocks the camera from the creature
Home				centers camera on creature
WASD				move the camera

hold F			show forces on creature
hold comma			show framerate

ctrl+P			take screenshot
ctrl+Q			exit game

Left/right bracket	speed up/slow down time
1-7				set time from 0% to 400%

F1				mute the music
F2				print creature json into logs
F4				clear file caches
F5				retry online connection
F6				Toggle between 60 and 30 FPS
F10				toggle drawing of the world/creatures (for performance)
F12				refresh graphics (if there's some kind of graphical glitch)

(note to self: if you update any important controls, remember to add them to the ingame cheat sheet)

===========================
 History
===========================

I ran a Kickstarter for Crescent Loom in early 2017 and got enough to work on it full-time for almost a year. Now, at the end of that, I'm entering a phase of open development where I'll be looking for more funding and continually updating it as time & money allows.

As far as I know, this is the very first easily-accessible biologically-realistic neural circuit simulator *in existence*. My goal is to make a tool that allows people to visualize neurons and form a mental model of how they actually work.

Fun fact: turns out basically nobody knows anything about neurons, so it's pretty much a level playing field. I've seen ten-year-olds have as much success (or more) in weaving brains as college students.

You can read more about the development of the game at wick.works/category/crescent-loom/

===========================
 Get Help
===========================
The game can be tricky to figure out! You have a couple options for assistance:

Ingame:
> Play the tutorial! There should be a little exclamation point over the icon on the map.
> There's some patterns you can follow (a lot like Lego instruction manuals). Press H to bring them up.
> Take a look at existing creatures by pressing Ctrl + S and loading them up.

Online:
> There's a basic overview over at crescentloom.com
> Ask a question (or show off your creature!) at reddit.com/r/crescentloom or on twitter @wickglyph
> Shoot me an email at wick@wick.works! I especially love to hear about tech problems & crashes. :)




===========================
 Why "Crescent Loom"?
===========================

"Crescent" actually shares the same root word as "create", "creature", "crescendo", or "increase"; it just means a growing thing. It was used to describe the moon in its waxing phase (waning-moon shapes are technically "decrescents", #nowyouknow) but eventually became associated with the shape. Since the game's about growing creatures, it seemed appropriate.

"Loom" because I'm going for romanticizing this whole practice as a form of ~weaving~ rather than engineering. My hope is that it'll make things more accessible and help drive home the point that _brains do not work like computers_. I've also always been a big fan of the story of Arachne.

Finally, I just think it's a pretty name. I went a little crazy churning through different possibilities: "neuroweaver", "neuronautica", "synaptical", "poke it with a stick", "ameobugs"... In the end, I decided to support my design objective of making it sound friendly and approachable over something more descriptive.

In retrospect, I may have overthought this.


===========================
Copyright 2017-21 wickworks
===========================


