var initialize = func()
{
	var cell = registercell();

	cell.name = 'Muscle';
	cell.desc = 'Controls a muscle.';

	cell.graphic = 'cell_motor.svg';

	//can't drag axons out of this; output-only
	cell.can_move = 1;
	cell.can_connect = 0;

	cell.capacitance = 2.5;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_big_na.mc');
};
