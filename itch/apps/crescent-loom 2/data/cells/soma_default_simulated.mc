var initialize = func()
{
	var cell = registercell();

	cell.name = 'Simulated';
	cell.desc = 'Action potentials in, action potentials out.';

	cell.graphic = 'cell1.svg';

	cell.capacitance = 4;
	cell.resistance = 1;

	cell.simulate_axons = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_na.mc');
};
