var initialize = func()
{
	var cell = registercell();

	cell.name = 'Harpoon';
	cell.desc = 'A handle for an array of cells which control the harpoon.';

	cell.graphic = 'cell_organ_harpoon.svg';

	cell.add_channel('channel_leak_big_k.mc');
	cell.add_channel('channel_leak_na.mc');

	//not a 'real' cell, just a hollowed-out thing that moves all the others around.
	cell.can_connect = 0;
	cell.can_accept_connections = 0;

	//tell it to make a number of other sensory cells (second number is direction: 0 = north, proceeds clockwise until 5 = north-west)
	cell.add_to_bundle('soma_motor.mc', 0); // fire
	cell.add_to_bundle('soma_sensory.mc', 1); // latched?
	cell.add_to_bundle('soma_motor.mc', 2); // release
};
