var initialize = func() {
	var cell = registercell();

	cell.name = 'Keyboard';
	cell.desc = 'Responds to keyboard input.';

	cell.graphic = 'cell_keyboard.svg';
	cell.text = 'nil';

	cell.capacitance = 4;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_na.mc');

	cell.add_variants('input', 'up', 'down', 'left', 'right');

	var key = cell.get_selected_option('input');
	if (key == 'up') { key_up(); }
	if (key == 'down') { key_down(); }
	if (key == 'left') { key_left(); }
	if (key == 'right') { key_right(); }
};

var key_up = func() {
	var cell = registercell();
	cell.add_channel('channel_keyboard.mc', 'w');
	cell.add_channel('channel_keyboard.mc', 'up');
	cell.text = '^';
};

var key_down = func() {
	var cell = registercell();
	cell.add_channel('channel_keyboard.mc', 's');
	cell.add_channel('channel_keyboard.mc', 'down');
	cell.text = 'v';
};

var key_left = func() {
	var cell = registercell();
	cell.add_channel('channel_keyboard.mc', 'a');
	cell.add_channel('channel_keyboard.mc', 'left');
	cell.text = '<';
};

var key_right = func() {
	var cell = registercell();
	cell.add_channel('channel_keyboard.mc', 'd');
	cell.add_channel('channel_keyboard.mc', 'right');
	cell.text = '>';
};
