var initialize = func()
{
	var cell = registercell();

	cell.name = 'Queer';
	cell.desc = 'Activates after being silenced.';

	cell.graphic = 'cell_queer.svg';

	cell.capacitance = 8;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_na.mc');

	cell.add_channel('channel_queer.mc');	//depolarizes on hyperpolarization
};
