var initialize = func()
{
	var cell = registercell();

	cell.name = 'Adaptor';
	cell.desc = 'Fires a burst when excited, but soon turns self off.';

	cell.graphic = 'cell_adaptor.svg';

	cell.capacitance = 8;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');
	cell.add_channel('channel_leak_big_k.mc');
	cell.add_channel('channel_leak_na.mc');

	cell.add_channel('channel_adaptor.mc');
	cell.add_channel('channel_adaptor_close.mc');
};
