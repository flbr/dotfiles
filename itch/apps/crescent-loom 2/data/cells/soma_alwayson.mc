var initialize = func()
{
	var cell = registercell();

	cell.name = 'Always-On';
	cell.desc = 'What it says on the label. Fires at a constant rate.';

	cell.graphic = 'cell_alwayson.svg';

	cell.capacitance = 8;
	cell.resistance = 10;

	cell.add_channel('channel_leak_big_k.mc');
	cell.add_channel('channel_leak_big_na.mc');

	//set up the options
	cell.add_variants('rate', 'weak', 'normal', 'rapid');
	cell.set_default_option('rate', 'normal');

	//implement the selected options for the current cell:
	var rate = cell.get_selected_option('rate');
	var na_leak_count = 0;
	if (rate == 'weak')	{ na_leak_count = 0; }
	if (rate == 'normal')	{ na_leak_count = 3; }
	if (rate == 'rapid') 	{ na_leak_count = 5; }
	for (var i = 1; i < na_leak_count; i++) {
		cell.add_channel('channel_leak_big_na.mc');
	}

	//set up the options
	cell.add_variants('adapt', 'none', 'weak', 'strong');
	cell.set_default_option('adapt', 'none');

	//implement the selected options for the current cell:
	var adapt = cell.get_selected_option('adapt');
	var adaptation_count = 0;

	if (adapt == 'none')	{ adaptation_count = 0; }
	if (adapt == 'weak')	{ adaptation_count = 1; }
	if (adapt == 'strong') 	{ adaptation_count = 2; }
	for (var i = 0; i < adaptation_count; i++) {
		cell.add_channel('channel_spikefreqadapt.mc');
	}
};
