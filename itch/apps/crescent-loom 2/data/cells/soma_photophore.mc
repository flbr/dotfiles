var initialize = func()
{
	var cell = registercell();

	cell.name = 'Photophore';
	cell.desc = 'Controls lights.';

	cell.graphic = 'cell_photophore.svg';

	//can't drag axons out of this; output-only
	cell.can_move = 1;
	cell.can_connect = 0;

	cell.capacitance = 2.5;
	cell.resistance = 1;

	//set up the options
	cell.add_variants('resting', 'on', 'off');
	cell.set_default_option('resting', 'on');

	//implement the selected options for the current cell:
	var rate = cell.get_selected_option('resting');
	if (rate == 'off')	{
		cell.add_channel('channel_leak_big_k.mc');
		cell.add_channel('channel_leak_big_k.mc');
		cell.add_channel('channel_leak_big_na.mc');
	}
	if (rate == 'on')	{
		cell.add_channel('channel_leak_big_k.mc');
		cell.add_channel('channel_leak_big_na.mc');
		cell.add_channel('channel_leak_big_na.mc');
	}
};
