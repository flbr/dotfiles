var initialize = func()
{
	var cell = registercell();

	cell.desc = 'dendrite';
	cell.type = dendrite;

	cell.capacitance = .75;
	cell.resistance = 10;

	cell.add_channel('channel_leak_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_small_na.mc');
};
