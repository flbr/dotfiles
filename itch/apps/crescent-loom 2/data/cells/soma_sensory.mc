var initialize = func()
{
	var cell = registercell();

	cell.name = 'Sensory';
	cell.desc = 'Activated by some kind of external stimuli.';
	cell.graphic = 'cell_sensory.svg';

	cell.capacitance = 2;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_na.mc');

	cell.add_channel('channel_sensory.mc');
};
