<By>
Olive Perry
link:wick.works


<Special Thanks>
Lucy Bellwood
Enyo Sherman
Sam Liebow
Adam P Farnsworth
Minat Jaravata
Laura Klinkner
Tyler Edwards
Jeffrey Brice
Portland Community
   College Media Lab
Joe Wasserman
Mark Chen

<Voices>
Jae Hero
Ken Yoshikawa

<Languages>
Cerberus X
link:cerberus-x.com

MiniC by Nicholas Grant
link:patreon.com/nrgsoft

<Font>
Rawengulk by Grzegorz Luk
link:glukfonts.pl

<Music & Sound>
Uluru by Kevin Macleod
link:incompetech.com

link:freesound.org

<Other Software>
Inkscape
link:inkscape.org
Paint.net
link:getpaint.net
Audacity
link:audacityteam.org
Scrivener
link:literatureandlatte.com
ScreenToGif
link:screentogif.com

<Quotes>
Mary Shelly
  Frankenstein
Karel Capek
  R.U.R.
Emily Short
  Galatea
Old Testament

<Kickstarter Backers>
Hunter Mayer
Gabriel
Allen & Martha Neuringer
Marc Falabregues
Chris Zaharia
Hayden Meyer
Joseph Dufour
The Marder Lab,
  Brandeis University
Dexorcist
Aya Maguire
Konrad Feiler
Barney
Caedmon "FancyPants" Webb
David Tyler Edwards
Perry
Mike Vignal
TheMaker51
Dee Faaborg
GamerQuest
Andrew Krause
John Kevin Sabino
Joe Glassy
Joshua "Renanse" Slack
Mica Peacock
Geek & Dad
Orestis Papaioannou
Meghan OConnell
Ed Stastny
Matthew Case
Kate Leitermann
K. Todd
Lorenzo Clark
Anna Henkin
Josh Boykin
Jason Hardwyre
Maggie Graham
hexmoire
Sam Liebow
Allison M
Mike Young
Phillip Pearson
Mikkel Snyder
Joy Mersmann
Tayler Stokes
Yori Kvitchko
Matt Robinson
Ken Yoshikawa
Hugh Owens
Bob Koutsky
Harrison
Bill Clark
Jules Chatelain
J. Kominsky
Richard 'Scylld' Comfort
Manuel Freiberger
Joe A. Wasserman
psadil
A. Lingawi
Scott Reichelt
Christo Meid
Christopher
   Auger-Dominguez
Skylar Troth
Nabil Maynard
Charles Emrich
M Pete
Benj Williams
Cary Staples
Housand JAM
Emily Merfeld
James Doe
Jackson Lango
Jason Leonard
Wesley James
Patrick Kimo
Robert Potter
Scott Lowrie
Corollarytower
Derek Outwater
Aquilasight
Anna and Matthew Dorsey
Zemerson
Lacey Van Nortwick
N. Reynolds
Shannon VanDyke
Timothy Storey
Dmitri Salcedo
Sam and Tyler Davidson
Laura Klinkner
Finn Ellis
Thomas Suckow
Connor Smith
Rebecca Groom
Benjamin Staffin
Amber Yust
Fraser K
Rebecca Nickerson
Cancy Chu
No thanks I don't want to
  be named I'm not
  declining this I want
  this whole thing put in
  the credits including
  this part
Shane Sullivan
Sean Harris
Ian M. Bone
Satchel Parker
Erin Hoffman-John
Ross Williams
Mark Ashton
Nick Cummings
Blake Werner
DW
James Miller
Kai and Kale Turner
Michael J. Gardner II
~
Jon Pepler
Sam Gordon
Louis Webb
dzil123
Fatmonkey
Lucy Bellwood
James LaRue
mcdanger
Dr. Lawton
Ben Sheff
John K Biro
mo
Luke T
chernobelenkiy
Mike Bundt
Ali R. Malek, M.D.
Arian Treffer
Emily Goldman
Markus Rosse
Owen Morgan
Traa VN
Billorko
Sara Bahmanyar
David Nyer
TJ "Daggaroth" Connolly
Adam Jones
Louis Garczynski
Petezah
Soren Jones
Alpha Quail
Sasha M
Jossef Osborn
A. Tamar Conner
Jon Dodson
Amper J.
Nic biondi
Karrin Blue
Craig M
Alex C
KLW
Brian Rosenberger
mrattus
Gina F.
Ifer
Dan DuVall
Rachel Moody
JH
Naomi Hemmings
moof
Adric "ArcaneZedric"
Brianna Roberts
Carlie & Matt Stolz
Ali Aras
Anthony
Nate Herrmann
Moth
Dr. Christoph Meisselbach
Cameron Merrick
James Parsons
Francis Botero
Merlin
Rob Abrazado
Carissa Hayden
Screwtape
Wes Brown
Marc7454
Zeo Cohen
JT Hughes
Padre E.T.
Katie L
Justen Palmer
Micha Stettler
cherie heiberg
Mark DeLoura
Malachi de AElfweald
Brandi Bryant
Meeghan Appleman
Justin Wilkinson
Ian M. Jenks
Erica Pettit
John Willcox-Beney
Humberto Eduardo
Krystal Leger
Patrick!
SilveryBeing
Jason Hall
Ingo Duarte
Anonymous
Simon H
Elena Churilov
Ian O'Dea
fabri mafessoni
Zombomb
Logan Tibbetts
N7Snake
Mattias Lehman
Robin Luft-Kite
Kaya Reed
Autumnnnnnn
Jordan Barnes
Doug M.
dr Piotr Migdal
Daryn
Beatscribe
Hap Hazard
Heather D. Fulton
Elena Churilov
Ian O'Dea
fabri mafessoni
Ajeeet

















