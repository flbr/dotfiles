var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Fiber';
	limb.desc = 'Trailing hair-like structure.';

	limb.category = decoration;	//basic | organs | senses | decoration
	//limb.icon = 'fiber_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	var branch = limb.add_svg('fiber.svg', limb.scale * .31);

	//aesthetic organs are intangible & near-massless
	limb.entire_organ(branch, 'aesthetic');
};