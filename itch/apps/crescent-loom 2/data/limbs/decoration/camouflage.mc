var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Camouflage';
	limb.desc = 'Get in touch with your inner kelp.';

	limb.category = decoration;	//basic | organs | senses | decoration
	//limb.icon = 'camouflage_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	var branch = limb.add_svg('camouflage.svg', limb.scale * .35);

	//aesthetic organs are intangible & near-massless
	limb.entire_organ(branch, 'aesthetic');
};