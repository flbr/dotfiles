var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Whisker';
	limb.desc = 'A sensitive fiber that can react to touch.';

	limb.category = senses;			//basic | organs | senses
	//limb.icon = 'whisker_icon';	//automatically defaults to this
};


var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('whisker.svg', limb.scale * .55);

	//set the drag of all branches ('normal' drag is 1.0)
	limb.set_drag(.2);

	//set the density of all branches ('normal' density is .05, going lower than .03 is danger)
	//limb.set_density(.1);

	//make all created branches unavailable for serving as a base for other limbs
	limb.cannot_attach_other_limbs();

	//make the whole thing a sensory organ
	limb.entire_organ(base, 'touch', 'cell_sensory.svg');
};
