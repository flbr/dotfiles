var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Eye';
	limb.desc = 'Activates when something blocks its line of sight.';

	limb.category = senses;		//basic | organs | senses
	//limb.icon = 'eye_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('eye.svg', limb.scale * .3);

	//set the drag of all branches that have been created so far
	limb.set_drag(.05);

	//set the density of all branches ('normal' density is .05)
	limb.set_density(.05);

	//make all created branches unavailable for serving as a base for other limbs
	limb.cannot_attach_other_limbs();
};
