var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Spine';
	limb.desc = 'Flexible section of bone.';

	limb.category = basic;		//basic | organs | senses
	//limb.icon = 'spine_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('spine.svg', limb.scale * .25);

	//set the drag of all branches that have been created so far
	limb.set_drag(2.0);
};
