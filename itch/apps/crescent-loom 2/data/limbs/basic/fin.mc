var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Fin';
	limb.desc = 'Tail with high drag. Helps move when on the end of a limb.';

	limb.category = basic;		//basic | organs | senses
	//limb.icon = 'fin_icon';	//automatically defaults to this

};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('fin.svg', limb.scale * .3);
	// limb.add_svg('fin_frills.svg', limb.scale * .3);

	limb.set_drag(2.0);

	limb.set_density(.05);

};