var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Hip';
	limb.desc = "Pair of springy appendages. They don't lie.";

	limb.category = basic;		//basic | organs | senses
	//limb.icon = 'hip_icon';	//automatically defaults to this

};


var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('hip.svg', limb.scale * .675);

	//set the drag of all branches ('normal' drag is 1.0)
	limb.set_drag(.5);

	//set the density of all branches ('normal' density is .05)
	limb.set_density(.05);
};
