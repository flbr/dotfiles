var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Nub';
	limb.desc = 'Bump with a muscle attachment point.';

	limb.category = basic;		//basic | organs | senses
	//limb.icon = 'nub_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('nub.svg', limb.scale * .7);
	//set the drag of all branches that have been created so far
	limb.set_drag(.25);
};