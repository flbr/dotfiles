var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Spindle';
	limb.desc = 'Rigid spindle with very low drag.';

	limb.category = basic;		//basic | organs | senses
	//limb.icon = 'spindle_icon';	//automatically defaults to this

};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('nub_spindle.svg', limb.scale * .7);
	//set the drag of all branches that have been created so far
	limb.set_drag(.1);
	// limb.set_drag(.01);
};
