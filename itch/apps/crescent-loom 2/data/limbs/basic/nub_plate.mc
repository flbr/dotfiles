var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Plate';
	limb.desc = 'Wide solid plate.';

	limb.category = basic;		//basic | organs | senses
	//limb.icon = 'plate_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('nub_wide.svg', limb.scale * .6);
	//set the drag of all branches that have been created so far
	limb.set_drag(.25);
};
