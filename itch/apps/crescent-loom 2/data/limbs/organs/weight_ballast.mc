var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Ballast';
	limb.desc = 'Heavy. Sinks downwards.';

	limb.category = organs;		//basic | organs | senses
	//limb.icon = 'ballast_icon';	//automatically defaults to this

	// limb.add_slider('Density%', .5, 2.0);
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('weight_ballast.svg', limb.scale * .5);

	limb.set_bouyancy(base, 2.5);

	//set the drag of all branches that have been created so far
	limb.set_drag(.15);
};
