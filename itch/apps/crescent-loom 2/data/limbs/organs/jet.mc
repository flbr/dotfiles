var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Jet';
	limb.desc = 'Squeeze to propel thyself with a stream of water.';

	limb.category = organs;		//basic | organs | senses
	//limb.icon = 'jet_icon';	//automatically defaults to this

};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('jet.svg', limb.scale * .4);

	//set the drag of all branches that have been created so far
	limb.set_drag(.5);

	//set the density of all branches ('normal' density is .05)
	limb.set_density(.0025);

	//make all created branches unavailable for serving as a base for other limbs
	limb.cannot_attach_other_limbs();
};
