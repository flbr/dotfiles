var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Balloon';
	limb.desc = 'Bouyant. Floats upwards.';

	limb.category = organs;		//basic | organs | senses
	//limb.icon = 'balloon_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('weight_balloon.svg', limb.scale * .4);

	//set any created organ neurons to use this graphic
	limb.set_neuron_graphic('cell_organ_balloon.svg');

	//make all created branches unavailable for serving as a base for other limbs
	limb.cannot_attach_other_limbs();

	//set the drag of all branches that have been created so far
	limb.set_drag(.15);
};
