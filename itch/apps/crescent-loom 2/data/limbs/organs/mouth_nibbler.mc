var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Nibbler';
	limb.desc = 'Tiny basic herbivorous mouth.';

	limb.category = organs;		//basic | organs | senses
	//limb.icon = 'molar_icon';	//automatically defaults to this
};


var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('mouth_nibbler.svg', limb.scale * .6);

	//set the drag of all branches ('normal' drag is 1.0)
	limb.set_drag(.1);

	//set the density of all branches ('normal' density is .05)
	limb.set_density(.01);

	//make all created branches unavailable for serving as a base for other limbs
	//limb.cannot_attach_other_limbs();
};