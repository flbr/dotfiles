
var setinfo = func() {
	var level = registerlevel();
	level.name = 'Open Sea | Area';
	level.desc = 'A large open area free of walls. Be careful not to get lost.';
	level.coord_x = .130;
	level.coord_y = -.350;
	level.icon = 'map_icon_area.svg';
};

var initialize = func() {
	var level = registerlevel();

 	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');
};

var update = func() {
	var level = registerlevel();
};

var event = func() {
	var level = registerlevel();
};
