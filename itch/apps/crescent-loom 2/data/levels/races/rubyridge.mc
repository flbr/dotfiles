var setinfo = func() {
	var level = registerlevel();
	level.name = 'Ruby Ridge';
	level.desc = 'Underwater canyons that make natural race-courses.';
	level.coord_x = -.01;
	level.coord_y =  .37;
	level.icon = 'map_icon_compass.svg';

	level.submap('map_rubyridge.png');
	level.submap_levels(
		'rubyridge/icon_rubyridge.mc',
		'rubyridge/level_choice.mc',
		'rubyridge/level_fishbowl.mc',
		'rubyridge/level_course.mc',
		'rubyridge/level_necked.mc',);
};

