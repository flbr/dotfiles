

//set the level info for menus, etc
var setinfo = func()
{
	var level = registerlevel();
	level.name = 'Loominologist HQ';
	level.desc = 'Training center for mending loominals.';
	level.coord_x = .38;
	level.coord_y = .47;
	level.icon = 'map_icon_hq.svg';

	level.start_highlight();
};

//initialize the level, once we actually start playing it
var initialize = func() {
	var level = registerlevel();

	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');

	//initialize the help script
	level.runscript('script_help.mc', 'initialize');

	// hide the patterns window even if it's set to open at start
	level.hide_pattern_window();

	// preload the tutorial voice lines
	level.load_lineset('tut_full');
};

//update the level, runs every loop
var update = func() {
	var level = registerlevel();

	level.runscript('script_help.mc', 'update');
};

var event = func() {
	var level = registerlevel();
};
