var setinfo = func() {
	var level = registerlevel();
	level.name = 'Loominologist HQ';
	level.desc = 'Training center for mending loominals.';
	level.coord_x = .38;
	level.coord_y = .47;
	level.icon = 'map_icon_hq.svg';

	//level.scenic();	//not clickable
	level.zooms_out();	//closes the submap when clicked
};

