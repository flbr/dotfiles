
var setinfo = func() {
	var level = registerlevel();
	level.name = 'Action Potentials | Wet Lab';
	level.desc = 'Examine a creature with visible action potentials.';
	level.coord_x = .66;
	level.coord_y = .71;
	level.icon = 'map_icon_obelisk.svg';
};

var initialize = func() {
	var level = registerlevel();

	level.live = 1;						//start live
	level.disable('can_toggle_live');	//lock it to be live

	level.disable('file_save');			//can't save creatures
	level.disable('file_load');			//can't load creatures
	level.disable('free_camera');		//can't move the camera around
	level.disable('edit_brain');		//can't modify the brain

 	//put down the background room
 	level.runscript('backgrounds/cave/cave.mc', 'initialize');

 	// -- load the map
	level.load_room('map');
	level.set_start('start');

 	level.add_creature('seasquirt', 'player', 'start');	//set the starting creature
	level.creature_disable_neurons('Pacemaker');
	level.creature_disable_neurons('Axon');

	level.timer_create('brain_prompt', 10.0);
 	level.var_brainopened = 'false';

	level.var_channelstate = 'disabled';	// all channels start disabled
};

var update = func() {
	var level = registerlevel();

	//once people discover that they can open the brain
	if (level.window_focus() == 0) {
		level.var_brainopened = 'true';

	//tell them to open the brain after a short waiting period
	} else {
		level.timer_tick('brain_prompt');
		if ((level.timer_current('brain_prompt') <= 0) && (level.var_brainopened == 'false')) {
			level.set_tutorialpointer('creature','');
		}
	}
};

// called when something happens the level script might want to know about
var event = func() {
	var level = registerlevel();
	if (level.event_name == 'menu_reset') {
		if (level.var_channelstate == 'disabled') {
			level.var_channelstate = 'enabled';
			level.creature_enable_neurons('Pacemaker');
			level.creature_enable_neurons('Axon');
		} else {
			level.var_channelstate = 'disabled';
			level.creature_disable_neurons('Pacemaker');
			level.creature_disable_neurons('Axon');
		}
	}

	// level.print('EVENT:');
	// level.print(level.event_name);
};
