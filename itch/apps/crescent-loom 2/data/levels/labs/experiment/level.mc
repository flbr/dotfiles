
var setinfo = func() {
	var level = registerlevel();
	level.name = 'Tracing | Wet Lab';
	level.desc = 'Perform experiments on a creature with hidden connections';
	level.coord_x = .66;
	level.coord_y = .81;
	level.icon = 'map_icon_obelisk.svg';
};

var initialize = func() {
	var level = registerlevel();

	level.live = 1;						//start live
	// level.disable('can_toggle_live')	//lock it to be live

	level.disable('creature_at_start');	//level doesn't give us a creature
	// level.disable('free_camera');	//can't move the camera around
	level.disable('edit_brain');		//can't modify the brain
	level.disable('edit_limbs');		//can't modify the body
	level.disable('file_save');			//can't save, can only load
	level.disable('creature_death');	//creautures don't die from death or dismemberment (for initial wait-for-loading)

	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');

 	// level.add_creature('peahen', 'player', 'start');	//set the starting creature

	level.timer_create('brain_prompt', 1.0);
	level.var_brainopened = 'false';

	level.var_creatureloaded = 'false';
};

var update = func() {
	var level = registerlevel();

	if (level.var_creatureloaded == 'false') {
		level.open_creature_file_modal();
		level.var_creatureloaded = 'loading';

	} else if (level.var_creatureloaded == 'loading') {
		// hang out here and wait until the loaded_creature event

	// once people discover that they can open the brain
	} else if (level.window_focus() == 0) {
		if (level.var_brainopened == 'false') { level.var_brainopened = 'true'; }

	// tell them to open the brain after a short waiting period
	} else if (level.var_brainopened == 'false') {
		level.timer_tick('brain_prompt');
		// if (level.timer_current('brain_prompt') <= 0) {
		// 	level.set_tutorialpointer('creature','', '','Open brain.');
		// }
	}
};

// called when something happens the level script might want to know about
var event = func() {
	var level = registerlevel();
	if (level.event_name == 'new_player_creature') {
		level.creature_hide_neurons('Axon');
		level.window_set_focus(0);			//start with brain open
		level.var_creatureloaded = 'true';

	} else if (level.event_name == 'menu_reset') {
		level.creature_reset_ions(); //in case they changed the ion concentrations
		level.creature_reset_optogenetics();
		level.creature_unablate_neurons();
		level.creature_enable_channels();//enables all channels

		level.show_text('Reset creature.');

	} else if (level.event_name == 'edit') {
		level.show_text('Reset creature.'); // yup, totally meant to do that

	} else if (level.event_name == 'menu_block_glu') {
		level.creature_disable_channels('Glu');
		level.show_text('CNQX applied. Excitation blocked.');

	} else if (level.event_name == 'menu_block_gaba') {
		level.creature_disable_channels('GABA');
		level.creature_jolt_neurons();
		level.show_text('Bicuculline applied. Inhibition blocked.');

	} else if (level.event_name == 'menu_block_all') {
		level.creature_disable_channels('VGSC');
		level.creature_disable_channels('GluR'); //temp: bug where passive conductance still lets synapses work with VGSCs disabled
		level.creature_disable_channels('GABAR');
		level.show_text('TTX applied. Action potentials blocked.');
	}
};
