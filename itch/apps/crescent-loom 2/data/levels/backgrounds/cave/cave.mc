

initialize = func() {
	var level = registerlevel();

	//------ STARTING LAYER PROPERTIES ------
	//multiplier for how to shift following layers with the camera in x / y
	level.set_parallax_zoom(.25);

	//------ ADD LAYERS TO THE BACKGROUND ------
	//name: file of the png
	//<SVG ONLY> layer name
	//layer number: serves as sorting & camera distance
	//position (can be omitted)
	//scale	(can be omitted)

	level.set_parallax(.05, 0);
	level.add_layer('cave.svg', 'bg0', -10, 0,50);

	level.set_parallax(.05, .03);
	level.add_layer('cave.svg', 'bg1', -9, 0,75);

	level.set_parallax(.10, .05);
	level.add_layer('cave.svg', 'bg2', -8, 0,100);

	level.set_parallax(.15, .07);
	level.add_layer('cave.svg', 'bg3', -7, 0,150);

	level.set_parallax(.25, .12);
	level.add_layer('cave.svg', 'bg4', -5, 0,250);

	level.set_parallax(.35, .15);
	level.add_layer('cave.svg', 'bg5', -4, 0,2350);
};
