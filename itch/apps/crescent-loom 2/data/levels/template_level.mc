
var setinfo = func() {
	var level = registerlevel();
	level.name = '%TEMPLATE_NAME%';
	level.desc = "The seaweed is always greener // in somebody else's lake";
	level.coord_x = 0.000;
	level.coord_y = 0.000;
	level.icon = 'map_icon_area.svg';
};

var initialize = func() {
	var level = registerlevel();

 	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');
};

var update = func() {
	var level = registerlevel();
};

var event = func() {
	var level = registerlevel();
};
