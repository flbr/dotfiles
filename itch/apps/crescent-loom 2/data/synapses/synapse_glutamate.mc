var initialize = func()
{
	var synapse = registersynapse();

	synapse.name = 'Excite';
	synapse.desc = 'Activates the connected neuron by releasing glutamate.';
	synapse.icon = 'icon_excite.png';

	synapse.set_channel_pre('channel_pre_glutamate.mc');	//releases neurotransmitter into the synapse
	synapse.set_channel_post('channel_post_glutamate.mc');	//responds to neurotransmitter in the synapse
};
