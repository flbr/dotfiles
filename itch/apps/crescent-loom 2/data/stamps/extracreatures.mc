
var initialize = func()
{
	var room = registerroom();

	room.nextgroup('empty','bubbled');	//'bubbled' is political: surrounded by polys whose filled status matches its own
		room.grouplimit = 3;			//only run rules in this group this many times

		room.nextrule(1);
		room.egg('');						//not giving it anything will pull from the creature server OR just the disc (if unavailable)

	room.run_rules();

};
