
var initialize = func()
{
	var room = registerroom();

	room.tileset = 'ocean';

	//room.nextgroup('empty','bubbled');	//'bubbled' is political: surrounded by polys whose filled status matches its own
	//	room.nextrule(1, 1);
	//	room.marker('start');
	//room.run_rules();

	// room.nextgroup('empty','bubbled');	//'bubbled' is political: surrounded by polys whose filled status matches its own
	// 	room.grouplimit = 1;			//only run rules in this group this many times

	// 	room.nextrule(1);
	// 	room.egg('');						//not giving it anything will pull from the creature server OR just the disc (if unavailable)

	// 	//room.nextrule(1);
	// room.run_rules();

	room.nextgroup('empty');
		room.nextrule(8);
		room.tile('algae');
		room.tile('algae');
		room.tile('algae', 'adjacent','open','bubbled');

		room.nextrule(12);
		room.tile('algae');

		room.nextrule(8);
		room.tile('fae');
		room.tile('fae');
		room.tile('fae');

		room.nextrule(12);
		room.tile('fae');

		room.nextrule(200);
	room.run_rules();


	room.nextgroup('floor','empty');
		room.nextrule(1);
		room.tile('boulder');
		
		room.nextrule(10);				//make boulders sorta uncommon
	room.run_rules();


	room.nextgroup('floor','empty');
		room.grouplimit = 2;			//only run rules in this group this many times

		room.nextrule(1);
		room.tile('faenest');
		
		room.nextrule(1);
	room.run_rules();

	
	room.include('scenery_seaweed.mc');
	room.include('scenery_walls.mc');
};
