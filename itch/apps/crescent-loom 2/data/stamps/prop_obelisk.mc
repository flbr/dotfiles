
var initialize = func()
{
	var room = registerroom();

	room.tileset = 'game';
	
	
	room.nextgroup('floor','empty','not_ceiling');
		room.grouplimit = 1;			//only run rules in this group this many times

		room.nextrule(1);
		room.tile('obelisk');
	room.run_rules();


	//temparary until we can add multiple stamps to rooms
	room.include('scenery_seaweed.mc');
	room.include('scenery_walls.mc');
};
