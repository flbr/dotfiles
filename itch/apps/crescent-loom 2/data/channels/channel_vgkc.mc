channel.name = 'VGKC';
channel.longname = 'Voltage-Gated Potassium Channel';//defaults to normal name if blank
channel.desc = 'Opens & strongly inhibits if cell exceeds 35mV. Closes once it gets below -70mV.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_k, 350);

// channel.set_opening_speed(0.20);
// channel.set_closing_speed(0.70);

//just open if we get high enough
channel.if_voltage_above(25);
channel.open();

//just close if we get low enough
channel.if_voltage_below(-70);
channel.close();
