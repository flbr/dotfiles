channel.name = 'Leak';
channel.longname = 'Leak K';
channel.desc = 'Always open. Maintains the baseline Vm.';

channel.set_conductance(ion_k, 16);
