channel.name = 'SFA';
channel.longname = 'Spike Frequency Adaptation';//defaults to normal name if blank
channel.desc = 'While excited, slowly opens & inhibits until returned to baseline.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_k, 500);

channel.set_opening_speed(0.0025);
channel.set_closing_speed(0.0025);

//let it fire for a bit before closing and ending the party
channel.if_voltage_above(-20);
channel.open();

// only close
channel.if_voltage_below(-60);
channel.close();
