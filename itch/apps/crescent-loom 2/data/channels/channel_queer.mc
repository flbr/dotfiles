channel.name = 'Queer';
channel.longname = 'Queer Current';//defaults to normal name if blank
channel.desc = 'When cell falls below -65 mV, excites after a delay.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 100);

//channel.if_voltage_below(-70);
//channel.open();

channel.if_closed();
channel.if_voltage_above(-68);
channel.reset_timer('opendelay');

channel.if_voltage_below(-60);
channel.unlock();

channel.if_closed();
channel.if_timer('opendelay', 35);
channel.reset_timer('open');
channel.open();

channel.if_timer('open', 120);
channel.close();
channel.lock();
