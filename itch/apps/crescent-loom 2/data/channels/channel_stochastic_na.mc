channel.name = 'Jitter';
channel.desc = 'Adds some noise.';

var conductance = 60;
if (channel.option == 'slight') { conductance = 20; }
if (channel.option == 'some') { conductance = 80; }
if (channel.option == 'lots') { conductance = 200; }

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, conductance);

channel.set_opening_speed(0.20);
channel.set_closing_speed(0.20);

// randomly open/close
channel.if_closed();
channel.if_random(.5);
channel.open();

channel.if_open();
channel.if_random(.5);
channel.close();
