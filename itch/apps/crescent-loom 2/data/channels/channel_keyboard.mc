channel.name = 'Key';
channel.longname = 'Keyboard Channel';//defaults to normal name if blank
channel.desc = 'Opens & excites the cell when a key is pressed.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 80);

//CLOSES if it's been open for too long
channel.if_timer('open', 10);
channel.close();

//given keypress opens the channel
if (channel.option != '') {
	channel.if_keyboard(channel.option);
	channel.open();
	channel.reset_timer('open');
}

