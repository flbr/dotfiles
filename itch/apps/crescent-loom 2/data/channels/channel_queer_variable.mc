channel.name = 'Queer';
channel.longname = 'Queer Current';//defaults to normal name if blank
channel.desc = 'Opens when cell falls below -80 mV. Closes after the cell reaches -40 mV.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 50);

var open_speed = .008;
if (channel.option == 'fast') { open_speed = .5; }
if (channel.option == 'normal') { open_speed = .008; }
if (channel.option == 'slow') { open_speed = .003; }

channel.set_opening_speed(open_speed);
channel.set_closing_speed(0.5);

channel.if_closed();
channel.if_voltage_below(-80);
channel.open();

channel.if_open();
channel.if_voltage_above(-40);
channel.close();
