channel.name = 'ON';
channel.longname = 'Always-on';//defaults to normal name if blank
channel.desc = 'Always open. Moderately excites the cell.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 100);

//always be open
channel.if_voltage_below(100);
channel.open();

