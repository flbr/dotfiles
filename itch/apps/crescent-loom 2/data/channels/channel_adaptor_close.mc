channel.name = 'Ad K';
channel.longname = 'Potassium Adaptor';//defaults to normal name if blank
channel.desc = 'Triggered by excitation after a long delay. Slowly opens & inhibits until returned to baseline.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_k, 400);

channel.set_opening_speed(0.0025);
channel.set_closing_speed(0.20);

//let it fire for a bit before closing and ending the party
channel.if_voltage_above(-75);
channel.open();

//whenever we go above reseting, resets the 'we-need-to-stay-at-resting' timer
channel.if_voltage_above(-75);
channel.reset_timer('closedelay');

// only close
channel.if_voltage_below(-75);
channel.if_timer('closedelay', 200);
channel.close();
// channel.reset_timer('opendelay');
