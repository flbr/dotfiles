channel.name = 'GluR';
channel.longname = 'Glutamate Receptor';//defaults to normal name if blank
channel.desc = 'Post-synaptic channel that excites the cell when open.';

channel.set_conductance(ion_na, 100);//ion, conductance (relative, buit shouldn't be crazy)

// channel.set_opening_speed(0.70);
channel.set_closing_speed(0.10);

channel.if_transmitter(glutamate);
channel.open();
channel.reset_timer('open');

channel.if_timer('open', 4);
channel.close();
