channel.name = 'Leak';
channel.longname = 'Leak: Small K';
channel.desc = 'Always open. Weakly maintains the baseline Vm.';

channel.set_conductance(ion_k, 4);
