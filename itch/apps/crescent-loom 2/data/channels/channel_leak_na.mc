channel.name = 'Leak';
channel.longname = 'Leak Na';
channel.desc = 'Always open. Maintains the baseline Vm.';

channel.set_conductance(ion_na, 16);
