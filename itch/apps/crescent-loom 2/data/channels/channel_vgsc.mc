channel.name = 'VGSC';
channel.longname = 'Voltage-Gated Sodium Channel';//defaults to normal name if blank
channel.desc = 'Opens & strongly excites if the cell exceeds -50mV. Closes & locks at 35mV until it gets back below -70mV.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 400);


//triggers to open when it goes above a threshold
channel.if_closed();
// channel.if_voltage_above(-60);
channel.if_random(-65, -10); //linearly increases in chance from first to second number
channel.open();
channel.reset_timer('_safety');

//CLOSES + inactivates if it depolarizes
channel.if_open();
channel.if_voltage_above(25);
channel.close();
channel.lock();

//CLOSES + inactivates if it's been open for too long (safety measure)
channel.if_open();
channel.if_timer('_safety', 8);
channel.close();
channel.lock();

//un-inactivates when it gets back low enough
channel.if_closed();
channel.if_voltage_below(-61);
channel.unlock();
