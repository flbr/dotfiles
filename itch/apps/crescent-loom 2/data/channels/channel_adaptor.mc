channel.name = 'Ad Na';
channel.longname = 'Sodium Adaptor';//defaults to normal name if blank
channel.desc = 'Triggered by excitation. Opens & excites for a period, then locks closed until inhibited.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 80);

//channel.set_conductance_closed(ion_k, 10);	//help the other phase out a little

//if we're closed and get excited, open up + excite the cell!
channel.if_closed();
channel.if_voltage_above(-70);
channel.open();
channel.reset_timer('open');

//once we've finished being open, close + lock
channel.if_open();
channel.if_timer('open', 200);
channel.close();
channel.lock();

//un-inactivates when it gets back low enough
channel.if_closed();
channel.if_voltage_below(-75);
channel.unlock();
