channel.name = 'VGKC';
channel.longname = 'Voltage-Gated Potassium Channel';//defaults to normal name if blank
channel.desc = 'Opens & inhibits if the cell gets too excited.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_k, 300);

channel.set_opening_speed(0.25);
channel.set_closing_speed(0.20);

//keep resetting the open delay until we get above a point
// channel.if_voltage_below(-48);
// channel.reset_timer('opendelay');

//once we have stopped constantly resetting the opendelay timer, open er up
// channel.if_timer('opendelay', 15);
// channel.open();

//just open if we get high enough
channel.if_voltage_above(0);
channel.open();


//just close if we get low enough
channel.if_voltage_below(-72);
channel.close();

//CLOSES if it gets back low enough
// channel.if_open();
// channel.if_voltage_above(-55);
// channel.reset_timer('closedelay');

// channel.if_timer('closedelay', 6);
// channel.close();
