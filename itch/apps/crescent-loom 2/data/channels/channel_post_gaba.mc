channel.name = 'GABAR';
channel.longname = 'GABA Receptor';//defaults to normal name if blank
channel.desc = 'Post-synaptic channel that inhibits the cell when open.';

channel.set_conductance(ion_k, 325);

// channel.set_opening_speed(0.70);
channel.set_closing_speed(0.10);

channel.if_transmitter(gaba);
channel.open();
channel.reset_timer('open');

channel.if_timer('open', 4);
channel.close();
