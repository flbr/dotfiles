channel.name = 'Leak';
channel.longname = 'Leak: Small Na';
channel.desc = 'Always open. Weakly maintains the baseline Vm.';

channel.set_conductance(ion_na, 1.5);
