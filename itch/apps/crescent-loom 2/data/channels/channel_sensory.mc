channel.name = 'Sens';
channel.longname = 'Sensory';
channel.desc = 'Opens & excites when triggered by a body part.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 100);

// all channels set as sensory are automatically opened by the corresponding body sensor.
// while recieving input, they constantly reset the first timer we declare here to keep it open
channel.sensory = 1;

channel.if_timer('open', 20);
channel.close();
