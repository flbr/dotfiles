
//shader for simple unlit sprites.

uniform sampler2D ColorTexture;

uniform float EffectLevel;
uniform float SolidR;
uniform float SolidG;
uniform float SolidB;
//uniform float SolidA;

void shader(){

	b3d_Ambient=texture2D( ColorTexture,b3d_Texcoord0 );
	
#if GAMMA_CORRECTION
	b3d_Ambient.rgb=pow( b3d_Ambient.rgb,vec3( 2.2 ) );
#endif

	vec4 target = vec4( SolidR, SolidG, SolidB, .5);//SolidA );

	//calculate midpoint color
	vec4 result = mix( b3d_Color, target, EffectLevel );
	
	b3d_Ambient *= result;		//apply vertex coloring

	b3d_Alpha = b3d_Color.a;	//extract alpha
}
