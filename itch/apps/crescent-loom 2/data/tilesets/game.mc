initialize = func()
{
	var set = registertileset();

	//======================== EGG ==================================================================
	set.newTileBatch('egg');
	set.class = 'egg';						//the generated item will be this special class
	set.body = 'dynamic';					//set will generate unmovable bodies [''|'static'|'dynamic']
	set.bouyancy = '.01';					//will sink slowly -- no sinking! they get stuck in the terrain!
	set.drag = '2.0';
	set.shiftposition = '7';				//shifts all the positions randomly
	set.rotation = 'scramble';				//placed at a random rotation
	set.scale = '0.75';
	//set.spritebehavior = 'heartbeat_slow';	//tile sprites behave thusly
	set.shiftcolor = '0.1';					//shifts all the colors randomly
	set.light = 'bigblue';					//will eventualyl call this script to make a light
	set.add('egg.svg');

	set.newTileBatch('eggpiece1');
	set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	set.behavior = 'decompose';
	set.bouyancy = '.5';				//will sink slowly
	set.drag = '1.0';
	set.material = 'meat';				//sfx, food type, etc.
	set.calories = '10';				//amount of energy gained by eating this
	set.scale = '0.65';
	set.shiftcolor = '0.1';				//shifts all the colors randomly
	set.spritebehavior = 'tint';		//tile sprites behave thusly
	set.add('eggpiece1.svg');

	set.newTileBatch('eggpiece2');
	set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	set.behavior = 'decompose';
	set.bouyancy = '.5';				//will sink slowly
	set.drag = '1.0';
	set.material = 'meat';				//sfx, food type, etc.
	set.calories = '10';				//amount of energy gained by eating this
	set.scale = '0.65';
	set.shiftcolor = '0.1';				//shifts all the colors randomly
	set.spritebehavior = 'tint';		//tile sprites behave thusly
	set.add('eggpiece2.svg');


	//======================== OBELISKS ==================================================================
	// set.newTileBatch('obelisk');
	// set.class = 'obelisk';				//the generated item will be this special class
	// set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	// set.bouyancy = '20';				//will sink, fast
	// set.drag = '1.5';					//rough, high drag
	// set.scale = '0.8';//'0.4';
	// set.light = 'hugeblue';				//will eventualyl call this script to make a light
	// set.add('obelisk.svg');
	//
	// set.newTileBatch('dome');
	// set.class = 'obelisk';				//the generated item will be this special class
	// set.body = 'static';				//set will generate unmovable bodies [''|'static'|'dynamic']
	// set.scale = '0.4';
	// set.light = 'hugeblue';				//will eventualyl call this script to make a light
	// set.add('dome.svg');
	//
	// //======================== CRYOPODS ==================================================================
	// set.newTileBatch('cryopod');
	// set.class = 'cryopod';				//the generated item will be this special class
	// set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	// set.bouyancy = '10';				//will sink, fast
	// set.drag = '1.5';					//rough, high drag
	// set.scale = '2.2';
	// set.add('cryopod.svg');
	//
	// set.newTileBatch('nemesis');
	// set.class = 'nemesis';				//the generated item will be this special class
	// set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	// set.bouyancy = '10';				//will sink, fast
	// set.drag = '1.5';					//rough, high drag
	// set.scale = '2.2';
	// set.add('nemesis.svg');


};
