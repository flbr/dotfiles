initialize = func()
{
	var set = registertileset();

	set.newTileBatch('boulder');
	set.body = 'dynamic';				//tile will generate unmovable bodies [''|'static'|'dynamic']
	set.bouyancy = '35';				//will sink.
	set.drag = '0.1';					//low drag so it can sink fast
	set.density = '0.01';				//default is 0.1
	set.rotation = 'scramble';			//placed at a random rotation
	set.scale = '0.5';
	set.shiftscale = '.25';				//shifts the scale of the created thing randomly
	set.add('boulder1');
	set.add('boulder2');


	set.newTileBatch('seaweed');			//sets the next category (clears any properties we're applying to new set)
	set.spritebehavior = 'kelpdrift';	//tile sprites behave thusly
	set.drawlayer = 'main_forefront,main_back';	//sets the draw layer to put these (multiple with ',' randomly distribute)
	//set.shiftcolor = '0.02';			//shifts all the colors randomly
	set.shiftbrightness = '0.04';		//shifts all the colors by the same random value
	set.shiftposition = '35';			//shifts all the positions randomly
	set.shiftscale = '.3';				//shifts the scale of the created thing randomly
	set.scale = '.8';
	set.add('spinykelp1');
	set.add('spinykelp2');
	set.add('spinykelp3');

	set.newTileBatch('rockdecor');
	set.drawlayer = 'static_front';		//sets the draw layer to put these
	set.rotation = 'scramble';			//placed at a random rotation
	set.shiftcolor = '0.1';				//shifts all the colors randomly
	set.shiftposition = '20';			//shifts all the positions randomly
	set.scale = '0.3';
	set.shiftscale = '.2';				//shifts the scale of the created thing randomly
	set.add('rockdecor1.png');
	set.add('rockdecor2.png');
	set.add('rockdecor3.png');
	set.add('rockdecor4.png');
	set.add('rockdecor5.png');
	set.add('rockdecor6.png');

	set.newTileBatch('biolumdecor');
	set.drawlayer = 'static_front';		//sets the draw layer to put these
	set.rotation = 'scramble';			//placed at a random rotation
	set.shiftcolor = '0.1';				//shifts all the colors randomly
	set.shiftscale = '.25';				//shifts the scale of the created thing randomly
	set.scale = '0.15';
	set.light = 'bioyellow';			//will eventualyl call this script to make a light
	set.lightlayer = 'main_front';
	set.add('biolumdecor1.png');
	set.add('biolumdecor2.png');
	set.add('biolumdecor3.png');

	set.newTileBatch('mushroom_blue_right');
	set.drawlayer = 'static_front';		//sets the draw layer to put these
	set.shiftcolor = '0.1';				//shifts all the colors randomly
	set.shiftscale = '.10';				//shifts the scale of the created thing randomly
	set.scale = '0.5';
	set.add('mushroom_blue_right_1');
	set.add('mushroom_blue_right_2');
	set.add('mushroom_blue_right_3');

	set.newTileBatch('fuzzypods');
	set.drawlayer = 'static_front';		//sets the draw layer to put these
	set.shiftcolor = '0.1';				//shifts all the colors randomly
	set.shiftscale = '.10';				//shifts the scale of the created thing randomly
	set.scale = '0.5';
	set.add('fuzzypods1');
	set.add('fuzzypods2');
	set.add('fuzzypods3');

	set.newTileBatch('algae');
	set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	set.bouyancy = '.05';				//will sink.
	set.drag = '1.5';					//rough, high drag
	set.rotation = 'scramble';			//placed at a random rotation
	set.scale = '0.2';
	set.shiftscale = '.25';				//shifts the scale of the created thing randomly
	set.spritebehavior = 'kelpdrift';	//tile sprites behave thusly
	set.shiftcolor = '0.2';				//shifts all the colors randomly
	set.shiftposition = '8';			//shifts all the positions randomly
	set.material = 'plant';				//sfx, food type, etc.
	set.calories = '10';				//amount of energy gained by eating this
	set.add('plant1');


	set.newTileBatch('faenest');
	set.class = 'faenest';				//the generated item will be this special class
	set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	set.bouyancy = '20';				//will sink, fast
	set.drag = '1.0';
	set.scale = '.65';
	//set.shiftscale = '.25';				//shifts the scale of the created thing randomly
	//set.shiftcolor = '0.4';				//shifts all the colors randomly
	//set.light = 'bigblue';				//will eventualyl call this script to make a light
	set.add('coralnest3');


	//## EDIBLE SNACKS ##//
	set.newTileBatch('fae');
	set.class = 'plankton';				//the generated item will be this special class
	set.body = 'dynamic';				//set will generate unmovable bodies [''|'static'|'dynamic']
	set.scale = '0.15';
	set.collisiongroup = '-255';		//sets the collision group of the object; negative numbers that match skip each other
	set.bump = '5.0';					//randomly bumps the object by this much on start
	set.drag = '0';						//skip the physics drag system; we'll fake it with behaviors
	set.spritebehavior = 'smoothburr';	//tile sprites behave thusly
	set.rotation = 'scramble';			//placed at a random rotation
	set.shiftcolor = '0.1';				//shifts all the colors randomly
	set.shiftposition = '7';			//shifts all the positions randomly
	set.shiftscale = '0.2';				//shifts the scale of the created thing randomly
	set.light = 'medblue';				//will eventualyl call this script to make a light
	set.material = 'plankton';
	set.calories = '10';				//amount of energy gained by eating this
	set.add('fae1');
};
