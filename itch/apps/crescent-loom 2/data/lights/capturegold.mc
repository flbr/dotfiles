var initialize = func() {
	var light = registerlight();

	light.scale = 3;
	light.alpha = 1.0;

	light.r = .91;
	light.g = .67;
	light.b = .19;
	
	light.pulseStrength = .1;
};
