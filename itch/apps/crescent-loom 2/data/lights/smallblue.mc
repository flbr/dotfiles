var initialize = func()
{
	var light = registerlight();

	light.scale = 0.25;
	light.alpha = 1.0;

	light.r = .2;
	light.g = .9;
	light.b = .9;

	light.pulseStrength = .1;
};
