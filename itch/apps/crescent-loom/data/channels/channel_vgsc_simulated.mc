channel.name = 'VGSC';
channel.longname = 'Voltage-Gated Sodium Channel';//defaults to normal name if blank
channel.desc = 'Opens & excites if the cell gets excited above -68 mV. Then, after a short delay, closes & locks until cell returns to baseline';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 500);

channel.set_opening_speed(0.9);
channel.set_closing_speed(0.5);

//triggers to open when it goes above a threshold
channel.if_closed();
channel.if_voltage_above(-68);
channel.open();
channel.reset_timer('open');

//CLOSES + inactivates if it's been open for too long
channel.if_open();
channel.if_timer('open', 11);
channel.close();
channel.lock();

//CLOSES + inactivates if it depolarizes
channel.if_open();
channel.if_voltage_above(10);
channel.close();
channel.lock();

//un-inactivates when it gets back low enough
channel.if_closed();
channel.if_voltage_below(-72);
channel.unlock();
