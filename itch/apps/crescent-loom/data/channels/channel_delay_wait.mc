channel.name = 'Wait';
channel.longname = '';//defaults to normal name if blank
channel.desc = 'When cell goes over -70mV, counteract it with inhibition for a short time.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_k, 625);

channel.set_opening_speed(1.0);
channel.set_closing_speed(0.01);

if (channel.option == 'short') { channel.set_closing_speed(0.02); }
if (channel.option == 'med') { channel.set_closing_speed(0.01); }
if (channel.option == 'long') { channel.set_closing_speed(0.005); }


//inhibit self in response to excitation
channel.if_closed();
channel.if_voltage_above(-70);
	channel.open();
	channel.reset_timer('open');

//start release the inhibition immediately after opening
channel.if_open();
channel.if_timer('open', 4);
	channel.reset_timer('refractory');
	channel.close();
	channel.lock();


//becomes available to re-open once it gets low enough
channel.if_closed();
channel.if_locked();
channel.if_voltage_below(-75);
channel.if_timer('refractory', 300);
	channel.unlock();
