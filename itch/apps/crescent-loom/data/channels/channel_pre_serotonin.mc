channel.name = '5HT';
channel.longname = 'Serotonin Vesicle';//defaults to normal name if blank
channel.desc = 'Pre-synaptic protein that releases neurotransmitter when excited.';

channel.set_opening_speed(0.20);
channel.set_closing_speed(0.20);

channel.if_voltage_above(-40);
channel.open();
channel.reset_timer('open');

channel.if_timer('open', 4);
channel.close();

channel.if_open();
channel.release_transmitter(serotonin);
