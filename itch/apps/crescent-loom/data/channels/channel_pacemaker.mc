channel.name = 'Burst';
channel.longname = 'Pacemaker Burst';
channel.desc = 'When cell goes over -57 mV, opens & keeps the cell excited for a period.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 170);

channel.start_closed();

var open_time = 100;
var close_speed = .025;
if (channel.option == 'short')  { close_speed = .040; open_time = 10; }
if (channel.option == 'normal') { close_speed = .025; open_time = 100; }
if (channel.option == 'long')   { close_speed = .018; open_time = 200; }

channel.set_opening_speed(.5);
channel.set_closing_speed(close_speed);

//triggers to open when it goes above a threshold
channel.if_closed();
channel.if_voltage_above(-57);
channel.open();
channel.reset_timer('open_time');

channel.if_open();
channel.if_timer('open_time', open_time);
// channel.if_voltage_above(-30);
channel.close();
channel.lock();

//un-inactivates after a while
channel.if_closed();
channel.if_voltage_below(-64);
channel.unlock();
