channel.name = 'NAR';
channel.longname = 'Noradrenaline Receptor';//defaults to normal name if blank
channel.desc = 'Post-synaptic channel that enhances other synapses when open.';

channel.set_modulation(1.0);	// this is further modified by the synapse strength & opening amount

// channel.set_opening_speed(0.70);
channel.set_closing_speed(0.10);

channel.if_transmitter(noradrenaline);
channel.open();
channel.reset_timer('open');

channel.if_timer('open', 4);
channel.close();
