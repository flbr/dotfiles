channel.name = 'Plat';
channel.longname = '';//defaults to normal name if blank
channel.desc = 'Maintains excitation; opens & excites when the cell is already excited.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 80);

//turn on
channel.if_closed();
channel.if_voltage_above(-50);
channel.open();

//turn off
channel.if_open();
channel.if_voltage_below(-45);
channel.close();