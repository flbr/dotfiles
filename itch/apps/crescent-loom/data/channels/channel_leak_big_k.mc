channel.name = 'Leak';
channel.longname = 'Leak: Big K';
channel.desc = 'Always open. Strongly maintains the baseline Vm.';

channel.set_conductance(ion_k, 75);
