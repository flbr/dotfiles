channel.name = 'Fire';
channel.longname = '';//defaults to normal name if blank
channel.desc = 'When cell goes over -70mV, wait a short time and then excite.';

// relative conductance of ions when channel is open
channel.set_conductance(ion_na, 120);

channel.set_opening_speed(0.01);
channel.set_closing_speed(0.5);

if (channel.option == 'short') { channel.set_opening_speed(0.02); }
if (channel.option == 'med') { channel.set_opening_speed(0.01); }
if (channel.option == 'long') { channel.set_opening_speed(0.002); }


//slowly excite self in response to excitation
channel.if_closed();
channel.if_unlocked();
channel.if_voltage_above(-70);
	channel.open();

// stay open for a little bit
channel.if_open();
channel.if_voltage_below(-40);
	channel.reset_timer('open');

// close when we're done
channel.if_open();
channel.if_timer('open', 60);
	channel.close();
	channel.lock();

//becomes available to re-open once it gets low enough
channel.if_closed();
channel.if_locked();
channel.if_voltage_below(-75);
	channel.unlock();
