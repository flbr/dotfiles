var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Orientation';
	limb.desc = 'Activates when held at a specific angle.';

	limb.category = senses;		//basic | organs | senses
	limb.icon = 'orientation_icon';

};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('orientation.svg', limb.scale * .3);

	//set the drag of all branches that have been created so far
	limb.set_drag(.05);

	//set the density of all branches ('normal' density is .05)
	limb.set_density(.05);
};
