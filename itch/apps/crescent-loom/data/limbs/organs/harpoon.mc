var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Harpoon';
	limb.desc = 'A dart that can stick to rocks... or prey.';

	limb.category = organs;		//basic | organs | senses
	//limb.icon = 'harpoon_icon';	//automatically defaults to this

};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('harpoon.svg', limb.scale * .5);

	//set the drag of all branches that have been created so far (normal drag is 1.0)
	limb.set_drag(0);

	//set the density of all branches ('normal' density is .05)
	limb.set_density(.01);

	//make all created branches unavailable for serving as a base for other limbs
	limb.cannot_attach_other_limbs();

};
