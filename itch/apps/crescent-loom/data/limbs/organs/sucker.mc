var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Sucker';
	limb.desc = 'Attaches on contact. Let go by silencing the neuron.';

	limb.category = organs;			//basic | organs | senses
	//limb.icon = 'sucker_icon';	//automatically defaults to this
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('sucker.svg', limb.scale * .5);

	//set any created organ neurons to use this graphic
	limb.set_neuron_graphic('cell_sensory_sucker.svg');

	//set the drag of all branches that have been created so far
	limb.set_drag(.05);

	//set the density of all branches ('normal' density is .05)
	limb.set_density(.05);

	//make all created branches unavailable for serving as a base for other limbs
	limb.cannot_attach_other_limbs();
};