var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Larynx';
	limb.desc = 'A crackling cry.';

	limb.category = organs;		//basic | organs | senses
	limb.icon = 'nub_icon';

	//limb.add_option('Croak', 'Larynx. A crackling cry.');
	//limb.add_option('Screal', 'Larynx. An aggressive cry.');
	//limb.add_option('Whisper', 'Larynx. A soft, subtle cry.');
};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	var larynx = limb.add_svg('nub.svg', limb.scale * 1.0);

	//the name of the larynx determines the sound it plays; it will look for 'sfx\call_<<name>>.wav'
	//var cry = '';
	//if (limb.name == 'Croak') 	{ cry = 'croak'; }
	//if (limb.name == 'Screal') 	{ cry = 'screal'; }
	//if (limb.name == 'Whisper')	{ cry = 'Whisper'; }
	//limb.organ(larynx, ('larynx_'+cry), 'cell_sensory.svg');
	limb.organ(larynx, 'larynx_croak', 'cell_sensory.svg');

	//make all created branches use their convex hulls for drag instead of what they literally are
	limb.set_usehullfordrag();//not needed since convex anyway?

	//set any created organ neurons to use this graphic
	limb.set_neuron_graphic('cell_organ_larynx.svg');

	//set the drag of all branches that have been created so far
	limb.set_drag(.05);
};
