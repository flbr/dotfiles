var metadata = func() {
	var limb = registerlimb();

	limb.name = 'Hinge';
	limb.desc = 'Loose pivot point.';

	limb.category = basic;		//basic | organs | senses
	//limb.icon = 'spine_icon';	//automatically defaults to this

};

var makebranches = func() {
	var limb = registerlimb();

	//make it from an svg (file, scale)
	limb.add_svg('hinge.svg', limb.scale * .33);

	//set the drag of all branches that have been created so far
	limb.set_drag(0.3);
};
