
uniform sampler2D ColorTexture;

uniform float SizeX;
uniform float SizeY;
uniform float DirectionX;
uniform float DirectionY;

void shader(){
	vec2 uv = b3d_Texcoord0.xy;

	//SIGMA = 3.0
	//KERNAL SIZE = 17

	vec4 color = vec4(0.0);

	vec2 off1 = vec2(DirectionX, DirectionY) / vec2(SizeX,SizeY);
	vec2 off2 = off1 * vec2(2.0);
	vec2 off3 = off1 * vec2(3.0);
	vec2 off4 = off1 * vec2(4.0);
	vec2 off5 = off1 * vec2(5.0);
	vec2 off6 = off1 * vec2(6.0);

	color += texture2D(ColorTexture, uv) * 0.197417;

	color += texture2D(ColorTexture, uv + off1) * 0.17467;
	color += texture2D(ColorTexture, uv - off1) * 0.17467;

	color += texture2D(ColorTexture, uv + off2) * 0.12098;
	color += texture2D(ColorTexture, uv - off2) * 0.12098;

	color += texture2D(ColorTexture, uv + off3) * 0.065592;
	color += texture2D(ColorTexture, uv - off3) * 0.065592;

	color += texture2D(ColorTexture, uv + off4) * 0.027835;
	color += texture2D(ColorTexture, uv - off4) * 0.027835;

	color += texture2D(ColorTexture, uv + off5) * 0.009245;
	color += texture2D(ColorTexture, uv - off5) * 0.009245;

	color += texture2D(ColorTexture, uv + off6) * 0.002403;
	color += texture2D(ColorTexture, uv - off6) * 0.002403;


	b3d_FragColor = color;
}
