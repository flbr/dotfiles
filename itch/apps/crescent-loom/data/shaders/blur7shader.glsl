
uniform sampler2D ColorTexture;

uniform float SizeX;
uniform float SizeY;
uniform float DirectionX;
uniform float DirectionY;

void shader(){

	//vec2 iResolution = vec2(1.0,1.0) / vec2(SizeX,SizeY);
	vec2 uv = b3d_Texcoord0.xy;

	//SIGMA = 1.5
	//KERNAL SIZE = 7

	vec4 color = vec4(0.0);

	vec2 off1 = vec2(DirectionX, DirectionY) / vec2(SizeX,SizeY);
	vec2 off2 = off1 * vec2(2.0);
	vec2 off3 = off1 * vec2(3.0);

	color += texture2D(ColorTexture, uv) * 0.266346;

	color += texture2D(ColorTexture, uv + off1) * 0.215007;
	color += texture2D(ColorTexture, uv - off1) * 0.215007;

	color += texture2D(ColorTexture, uv + off2) * 0.113085;
	color += texture2D(ColorTexture, uv - off2) * 0.113085;

	color += texture2D(ColorTexture, uv + off3) * 0.038735;
	color += texture2D(ColorTexture, uv - off3) * 0.038735;


	b3d_FragColor = color;
}
