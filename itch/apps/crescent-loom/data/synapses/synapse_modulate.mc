var initialize = func()
{
	var synapse = registersynapse();

	synapse.name = 'Modulate';
	synapse.desc = 'Increases the strength of nearby synapes by releasing noradrenaline.';
	synapse.icon = 'icon_modulate.png';

	//set up the options
	synapse.add_variants('effect', 'enhance', 'suppress');
	synapse.set_default_option('effect', 'enhance');		//will otherwise default to the first one

	var effect = synapse.get_selected_option('effect');
	if (effect == 'enhance') {
		synapse.set_channel_pre('channel_pre_noradrenaline.mc');
		synapse.set_channel_post('channel_post_noradrenaline.mc');
	}
	if (effect == 'suppress') {
		synapse.set_channel_pre('channel_pre_serotonin.mc');
		synapse.set_channel_post('channel_post_serotonin.mc');
	}
};
