var initialize = func()
{
	var synapse = registersynapse();

	synapse.name = 'Inhibit';
	synapse.desc = 'Silences the connected neuron by releasing GABA.';
	synapse.icon = 'icon_inhibit.png';

	synapse.set_channel_pre('channel_pre_gaba.mc');		//releases neurotransmitter into the synapse
	synapse.set_channel_post('channel_post_gaba.mc');	//responds to neurotransmitter in the synapse
};
