
var setinfo = func() {
	var level = registerlevel();
	level.name = 'Caverns | Area';
	level.desc = 'Open cavern surrounded by tight tunnels.';
	level.coord_x = -.03;
	level.coord_y = -.72;
	level.icon = 'map_icon_area.svg';
};

var initialize = func() {
	var level = registerlevel();

 	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');
};

var update = func() {
	var level = registerlevel();

};

var event = func() {
	var level = registerlevel();
};
