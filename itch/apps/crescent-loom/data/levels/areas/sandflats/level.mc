
var setinfo = func() {
	var level = registerlevel();
	level.name = 'Sand Flats | Area';
	level.desc = 'A desolate, empty seabed. Useful as a test-ground.';
	level.coord_x = .700;
	level.coord_y = .700;
	level.icon = 'map_icon_area.svg';
};

var initialize = func() {
	var level = registerlevel();

 	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
 	level.runscript('backgrounds/cave/cave.mc', 'initialize');
};

var update = func() {
	var level = registerlevel();

};

var event = func() {
	var level = registerlevel();
};
