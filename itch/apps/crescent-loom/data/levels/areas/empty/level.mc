
var setinfo = func() {
	var level = registerlevel();
	level.name = 'Empty | Area';
	level.desc = 'Literally nothing. Used for testing';
	level.coord_x = .230;
	level.coord_y = -.350;
	level.icon = 'map_icon_area.svg';
};

var initialize = func() {
	var level = registerlevel();

 	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');
};

var update = func() {
	var level = registerlevel();
};

var event = func() {
	var level = registerlevel();
};
