
//set the level info for menus, etc
var setinfo = func()
{
	var level = registerlevel();
	level.name = 'Sewee Bay | Area';
	level.desc = 'Explore one of the few remaining pre-Rubicon ecosystems.';
	level.coord_x = .450;
	level.coord_y = -.130;
	level.icon = 'map_icon_area.svg';
};

//initialize the level, once we actually start playing it
var initialize = func() {
	var level = registerlevel();

 	// -- load the map
	level.load_room('map');
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');
};

//update the level, runs every loop
var update = func() {
	var level = registerlevel();
};

var event = func() {
	var level = registerlevel();
};
