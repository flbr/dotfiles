
var setinfo = func() {
	var level = registerlevel();
	level.name = 'Ruby Ridge | Race';
	level.desc = 'Choose creatures to race through a narrow canyon.';
	level.coord_x = -.01;
	level.coord_y =  .37;
	level.icon = 'map_icon_area.svg';
};

var initialize = func() {
	var level = registerlevel();

	// -- load the map
	level.load_room('map'); // room_straight
	level.set_start('start');

	//add the background cave layers
	level.runscript('backgrounds/cave/cave.mc', 'initialize');

	// initialize the race (after map load so it can access the markers)
	level.runscript('races/script_race.mc', 'initialize');
};

var update = func() {
	var level = registerlevel();
	level.runscript('races/script_race.mc', 'update');
};

// called when something happens the level script might want to know about
var event = func() {
	var level = registerlevel();
	level.runscript('races/script_race.mc', 'event');
};
