
// call this ~ AFTER ~ loading the map so it has access to the markers
var initialize = func() {
	var level = registerlevel();

	// ===== game settings ====== //
	level.disable('can_drag');				//can't drag creatures around
	level.disable('creature_at_start');		//level doesn't give us a creature
	level.disable('creature_on_pause');		//level doesn't give us a creature
	level.disable('creature_is_hatched');	//creatures start as eggs
	level.disable('edit_brain');			//can't modify the brain
	level.disable('edit_limbs');			//can't modify the body
	level.disable('egg_babies');			//things hatch as full adults
	level.disable('egg_laying');			//creatures can't lay eggs
	level.disable('file_save');				//can only load, can't save
	// level.disable('free_camera');		//can't move the camera around

	level.maximum_zoom = 30; 				//can zoom out a lot (default is 20)
	level.enable_waveforce = 0;				//force sometimes is pointing in the wrong way at start, so removing for races

	// ===== script variables ====== //
	//don't start looking for win/lose conditions until things have hatched
	level.var_hatched = 'false';
	//nothing has won
	level.var_won = 'false';

	//make a timer to hatch all eggs
	level.timer_create('hatch', 3.2);//2.2); // same as the run_countdown, below
	level.timer_loop('hatch', 0);

	 //max time for each race
	level.timer_create('maxtime', 3600.0); // an hour
	level.timer_loop('maxtime', 0);

	// ====== MAP REQUIREMENTS ======= //
	// four markers labeled 'start1' through 'start4'
	// one marker titled 'raceend' to put the beacon
	level.add_beacon('raceend');

	level.open_contestant_modal();
};



//update the level, runs every loop
var update = func() {
	var level = registerlevel();

	//follow all creatures/eggs
	level.camera_follows_everything();

	//have things hatched yet? i.e. are we looking for a win condition
	if (level.var_hatched == 'true') {
		//show the timer
		// level.set_objective('maxtime');

		//if we run out of time, end the level (now unlikely to happen since it's like an hour)
		if (level.timer_tick('maxtime') > 0) {
			level.var_won = 'Out of time!';
		}

		if (level.var_won == 'false') {
			//if only one creature is left, end the level
			if (level.count_creatures() <= 1) {
				level.var_won = "A carnivore's victory";
			}
		}

		//if something reaches the finish line, hoover them up, end the level, and remove them from being tracked
		if (check_if_creature_has_won(level.var_creature_id_1)) { level.var_creature_id_1 = ''; }
		if (check_if_creature_has_won(level.var_creature_id_2)) { level.var_creature_id_2 = ''; }
		if (check_if_creature_has_won(level.var_creature_id_3)) { level.var_creature_id_3 = ''; }
		if (check_if_creature_has_won(level.var_creature_id_4)) { level.var_creature_id_4 = ''; }
	}

	// show the end-of-level screen
	if (level.var_won !== 'false') {
		level.complete(level.var_won);
	}

	//hatch all eggs?
	if (level.timer_tick('hatch') > 0) {
		level.hatch_all_eggs();
		level.var_hatched = 'true';
	}
};

// if the given creature ID is in range of the beacon, capture the creature and wrap up the race
var check_if_creature_has_won = func(creature_id) {
	if (creature_id !== '') {
		var distance = level.check_distance('raceend', creature_id);
		if (distance < 11) {
			var creature_name = level.get_creature_name(creature_id);

			// set the win message if it hasn't been yet
			if (level.var_won == 'false') { level.var_won = creature_name + ' wins!'; }
			// add them & their time to the race-end screen
			level.add_contestant_to_endscreen(creature_name, level.timer_elapsed('maxtime'));
			// hoover them up into a golden ball of light
			level.beacon_capture('raceend', creature_id);
			return true;
		}
	}
	return false;
};

// called when something happens the level script might want to know about
var event = func() {
	var level = registerlevel();
	if (level.event_name == 'edit') {
		level.open_contestant_modal();

	} else if (level.event_name == 'contestants_chosen') {
		reset_race();	// start the race
		level.live = 1;	// go live (in case the window was brought up by pausing)

	} else if (level.event_name == 'level_complete_next') {
		reset_race(); // start the race

	} else if (level.event_name == 'level_complete_back') {
		level.open_contestant_modal();
	}
};

// adds a creature from the contestant list & returns the ID
var add_contestant = func(contestant_index) {
	var marker_array = ['start1','start2','start3','start4'];
	var marker = marker_array[contestant_index];

	var creature_id = level.add_contestant_creature(contestant_index, marker);
	return creature_id;
};


var reset_race = func() {
	level.uncomplete(); 		// deactivate the end-of-level gui
	level.order66(); 			// clear all creatures + eggs
	level.set_objective('');	// clear the timer

	// make the given creatures, recording their IDs
	level.var_creature_id_1 = add_contestant(0);
	level.var_creature_id_2 = add_contestant(1);
	level.var_creature_id_3 = add_contestant(2);
	level.var_creature_id_4 = add_contestant(3);

	level.timer_reset('hatch');
	level.timer_reset('maxtime');

	level.var_hatched = 'false';
	level.var_won = 'false';

	level.run_countdown(2.6);

	level.print(' ');
	level.print('------------------- resetting race --------------------');
};
