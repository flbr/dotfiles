var setinfo = func() {
	var level = registerlevel();
	level.name = 'Loominologist HQ';
	level.desc = 'Training center for mending loominals.';
	level.coord_x = .38;
	level.coord_y = .47;
	level.icon = 'map_icon_hq.svg';

	level.start_highlight();

	level.submap('map_cuba.png');
	level.submap_levels(
		'headquarters_icon.mc',
		'actionpotentials/level.mc',
		'full/level.mc',
		'01_stimulate/level.mc',
		'02_connect/level.mc',
		// '03_radiation/level.mc',
		'04_toybox/level.mc',
		'05_delay/level.mc',
		'06_senses/level.mc',
		'07_nest/level.mc');
};

