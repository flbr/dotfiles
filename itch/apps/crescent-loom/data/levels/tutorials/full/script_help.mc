
//initialize the level, once we actually start playing it
var initialize = func()
{
	var level = registerlevel();

	level.var_done = 'false';

	level.var_muscletip = 'false';
	level.var_motortip = 'false';
	level.var_openedbrain = 'false';

	level.var_savetip = 'false';

	//first lineset we load will be the default we'll use if play_line isn't given one
	level.load_lineset('tut_full');

};


//update the level, runs every loop
var update = func() {
	var level = registerlevel();

	//guide the player put down a body
	if (level.var_done == 'false') {
		level.play_line('01_thisistheloom');

		if (level.creature_has_part('spine') < 4) {
			//if we prematurely opened the brain
			if (level.window_focus() == 0) {
				level.set_tutorialpointer('creature','');
			} else {
				level.play_line('02_firstthingsfirst');
				if (level.line_playing() == 0) {
					level.set_tutorialpointer('body','spine');
				} else {
					level.hide_tutorialpointer();
				}
			}

		} else if (level.creature_has_part('fin') < 1) {
			if (level.window_focus() == 0) {
				level.set_tutorialpointer('creature','');
			} else {
				level.set_tutorialpointer('body','fin');
				level.play_line('03_wewontgetfar');
			}

		} else if ((level.creature_has_muscle() < 2) || ((level.mouse_down() == 1) && (level.var_openedbrain == 'false'))) {
			if (level.window_focus() == 0) {
				level.set_tutorialpointer('body','muscle');

			} else if (level.editor_category() != 'muscles') {
				level.set_tutorialpointer('body','muscle');
				level.play_line_interrupt('04_musclesgivemotion');

			//once they click the muscle mode
			} else if ((level.creature_has_muscle() == 0) && (level.editor_category() == 'muscles') && (level.mouse_down() == 0))  {
				level.set_gif_popout('controls/newstitch_numbered_small.gif', 'Stitching muscles');

			} else {
				level.set_tutorialpointer('body','muscle');
				level.play_line('05_rememberonelongmuscle');
			}

		//if we're focusing on the body
		} else if (level.window_focus() == 1) {
			level.set_gif_popout('');
			level.set_tutorialpointer('creature','');
			level.play_line('06_thebrainiswhere');

		// === OPEN BRAIN STAGE ===
		} else if (level.var_openedbrain == 'false') {
			level.var_openedbrain = 'true';
			level.play_line_interrupt('07_thoseredcells');
			level.hide_tutorialpointer();

		} else if (level.creature_has_neuron('pacemaker') == 0) {
			level.set_tutorialpointer('cells','pacemaker');

			//before they select the placemaker
			if (level.brain_painting() !== 'pacemaker') {
				level.play_line('08_easiestwaytoget');

			//after they've selected the placemaker
			} else {
				level.play_line('09_putonedown');
			}

		//} else if (level.creature_has_synapse('excite', 'pacemaker') >= 1) {
		//	level.set_tutorialpointer('circuit','excite', '','Oops! Other way around. Right-click to delete.');
		//	level.play_line_interrupt('react_otherwayaround');

		} else if (level.creature_has_synapse('excite') == 0) {
			level.play_line('10_andthenconnectit');

			if (level.mouse_down() == 0) {
				level.set_tutorialpointer('circuit','pacemaker');
			} else {
				level.hide_tutorialpointer();
			}

		} else if (level.creature_has_neuron('pacemaker') < 2) {
			level.set_tutorialpointer('cells','pacemaker');
			level.play_line('10b_niceonemore');

		} else if (level.creature_has_synapse('excite') < 2) {
			level.set_tutorialpointer('circuit','pacemaker');

		} else if (level.live == 0) {
			level.set_tutorialpointer('hud','play');
			level.play_line('11_letsseeitgo');

		}

		if (level.live == 1) {
			level.hide_tutorialpointer();
			level.var_done = 'true';
			//level.play_line('frankenstein_ibeheldthewretch','passage');
			level.play_line('frankenstein_accursedcreator','passage');
		}

	//if we've gone live and set var_done to 'true'
	// } else {
	// 	//once we go back to edit, show a tooltip to save/load
	// 	if (level.live == 0) {
	// 		if (level.var_savetip == 'false') {
	// 			level.set_tutorialpointer('hud','patterns');
	//
	// 			//detect this in a super hacky way: just detects if window was opened.
	// 			if (level.creature_is_saved() == 1) {
	// 				level.hide_tutorialpointer();
	// 				level.var_savetip = 'true';
	// 			}
	// 		}
	// 	}
	}

	if (level.live == 1) {
		level.play_line('frankenstein_ibeheldthewretch','passage');
	}
};
