
var initialize = func()
{
	var room = registerroom();

	room.tileset = 'ocean';
	
	//apply props and stuff to this room

	// be able to guarantee that a thing will happen once if possible
	// be able add a tile to X proportion of times some condition is filled
	// be able to have rules be mutually exclusive : can't place a tile on a poly if something else is already there
	//  >>> did this in rubicon with a 'layers' system. all rules added until we hit a layer are mutually exclusive.
	//    >>> if there's only one rule but you want it to only happen some of the time, can just add an empty rule

	room.nextgroup('filled');
		room.nextrule(2);
		room.tile('rockdecor');
		room.tile('rockdecor', 'adjacent','filled');
		room.tile('rockdecor', 'adjacent','filled');

		room.nextrule(6);
		room.tile('rockdecor');

		room.nextrule(1);
		room.tile('biolumdecor');
		room.tile('biolumdecor', 'adjacent','backdrop');
		room.tile('biolumdecor', 'adjacent','backdrop');

		room.nextrule(3);
		room.tile('biolumdecor');

		room.nextrule(70);	
	room.run_rules();

	//room.nextgroup('backdrop');
	//	room.nextrule(1);
	//	room.tile('biolumdecor');
	//	room.tile('biolumdecor', 'adjacent','backdrop');
	//	room.tile('biolumdecor', 'adjacent','backdrop');
//
//		room.nextrule(3);
//		room.tile('biolumdecor');
//
//		room.nextrule(35);	
//	room.run_rules();


};
