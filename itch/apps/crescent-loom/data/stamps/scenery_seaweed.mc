
var initialize = func()
{
	var room = registerroom();

	room.tileset = 'ocean';
	
	//apply props and stuff to this room

	// TODO:
	// be able to guarantee that a thing will happen once if possible
	// be able add a tile to X proportion of times some condition is filled
	// be able to have rules be mutually exclusive : can't place a tile on a poly if something else is already there
	//  >>> did this in rubicon with a 'layers' system. all rules added until we hit a layer are mutually exclusive.
	//    >>> if there's only one rule but you want it to only happen some of the time, can just add an empty rule

	room.nextgroup('floor_island','filled');
		room.nextrule(4);				//makes a new result with x propability
		room.tile('seaweed');			//adds some seaweed to the selected polygon
		room.tile('seaweed');
		room.tile('seaweed');

		room.nextrule(2);				//makes a new result with x propability
		room.tile('seaweed');			//adds some seaweed to the selected polygon
		room.tile('seaweed');
		room.tile('seaweed', 'adjacent','filled','floor'); //... and to some of its neighbors
		room.tile('seaweed', 'adjacent','filled','floor');

		room.nextrule(10);				//empty rule
	room.run_rules();
	
};
