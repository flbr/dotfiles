
var initialize = func()
{
	var room = registerroom();

	room.tileset = 'ocean';
	
	//apply props and stuff to this room

	// be able to guarantee that a thing will happen once if possible
	// be able add a tile to X proportion of times some condition is filled
	// be able to have rules be mutually exclusive : can't place a tile on a poly if something else is already there
	//  >>> did this in rubicon with a 'layers' system. all rules added until we hit a layer are mutually exclusive.
	//    >>> if there's only one rule but you want it to only happen some of the time, can just add an empty rule


	room.nextgroup('center');	//find the tile closest to the center of the given group
		room.nextrule(1, 1);
		room.marker('start');
	room.run_rules();

	room.include('scenery_walls.mc');
};
