
var initialize = func()
{
	var room = registerroom();

	room.tileset = 'ocean';
	
	//apply props and stuff to this room

	// be able to guarantee that a thing will happen once if possible
	// be able add a tile to X proportion of times some condition is filled
	// be able to have rules be mutually exclusive : can't place a tile on a poly if something else is already there
	//  >>> did this in rubicon with a 'layers' system. all rules added until we hit a layer are mutually exclusive.
	//    >>> if there's only one rule but you want it to only happen some of the time, can just add an empty rule

/*
	room.nextgroup('empty','bubbled');	//'bubbled' is political: surrounded by polys whose filled status matches its own
		room.nextrule(8);
		room.tile('algae');
		room.tile('algae');
		room.tile('algae', 'adjacent','open','bubbled');

		room.nextrule(12);
		room.tile('algae');

		room.nextrule(8);
		room.tile('fae');
		room.tile('fae');
		room.tile('fae');

		room.nextrule(12);
		room.tile('fae');

		room.nextrule(120);
	room.run_rules();
*/	

	room.include('scenery_seaweed.mc');
	room.include('scenery_walls.mc');
};
