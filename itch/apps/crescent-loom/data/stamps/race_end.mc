
var initialize = func()
{
	var room = registerroom();

	room.tileset = 'game';
	
	
	room.nextgroup('position:.75,.5');		//tries to position somewhere in the room
		room.grouplimit = 1;				//only run rules in this group this many times

		room.nextrule(1);
		room.tile('dome');
	room.run_rules();


	//temparary until we can add multiple stamps to rooms
	room.include('scenery_seaweed.mc');
	room.include('scenery_walls.mc');
};
