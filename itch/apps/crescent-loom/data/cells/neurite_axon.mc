var initialize = func()
{
	var cell = registercell();

	cell.desc = 'axon';
	cell.type = axon;

	cell.capacitance = .4;
	cell.resistance = 20;

	cell.add_channel('channel_leak_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_small_na.mc');

	cell.add_channel('channel_vgsc.mc');		//voltage-gated sodium channel for ACTION POTENTIALS
	cell.add_channel('channel_vgkc.mc');		//voltage-gated potassium channel, helps bring AP back down
};
