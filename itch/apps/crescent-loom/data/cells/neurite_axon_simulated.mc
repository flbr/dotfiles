var initialize = func()
{
	var cell = registercell();

	cell.desc = 'axon';
	cell.type = axon;

	cell.capacitance = .75;
	cell.resistance = 20;

	cell.add_channel('channel_leak_k.mc');
	cell.add_channel('channel_leak_small_na.mc');

	cell.add_channel('channel_vgsc_simulated.mc');		//voltage-gated sodium channel for ACTION POTENTIALS
	cell.add_channel('channel_vgkc_simulated.mc');		//voltage-gated potassium channel, helps bring AP back down
};
