var initialize = func()
{
	var cell = registercell();

	cell.name = 'Plateau';
	cell.desc = 'Excite it to turn on, inhibit to turn off.';

	cell.graphic = 'cell_plateau.svg';

	cell.capacitance = 8;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');
	cell.add_channel('channel_leak_na.mc');

	cell.add_channel('channel_plateau.mc');
};
