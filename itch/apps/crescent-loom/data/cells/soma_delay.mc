var initialize = func() {
	var cell = registercell();

	cell.name = 'Delay';
	cell.desc = 'Fires after a brief delay.';

	cell.graphic = 'cell_delay_1.svg';

	cell.capacitance = 4;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_small_na.mc');

	cell.add_variants('delay', 'short', 'medium', 'long');
	cell.set_default_option('delay', 'medium');		//will otherwise default to the first one

	//implement the selected options for the current cell:
	var delay = cell.get_selected_option('delay');
	if (delay == 'short')	{ delay_1(); }
	if (delay == 'medium')	{ delay_2(); }
	if (delay == 'long')	{ delay_3(); }
};

var delay_1 = func() {
	var cell = registercell();
	cell.add_channel('channel_delay_wait.mc', 'short');
	cell.add_channel('channel_delay_fire.mc', 'short');
	cell.graphic = 'cell_delay_1.svg';
};

var delay_2 = func() {
	var cell = registercell();
	cell.add_channel('channel_delay_wait.mc', 'med');
	cell.add_channel('channel_delay_fire.mc', 'med');
	cell.graphic = 'cell_delay_2.svg';
};

var delay_3 = func() {
	var cell = registercell();
	cell.add_channel('channel_delay_wait.mc', 'long');
	cell.add_channel('channel_delay_fire.mc', 'long');
	cell.graphic = 'cell_delay_3.svg';
};
