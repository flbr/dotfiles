var initialize = func()
{
	var cell = registercell();

	cell.name = 'Sensory';
	cell.desc = 'Activated by some kind of external stimuli.';
	cell.graphic = 'cell_sensory.svg';

	cell.capacitance = 2;
	cell.resistance = 1;

	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_big_k.mc');
	cell.add_channel('channel_leak_small_na.mc');

	//three of these so organs can individually turn them on
	cell.add_channel('channel_sensory_weak.mc');
	cell.add_channel('channel_sensory_weak.mc');
	cell.add_channel('channel_sensory_weak.mc');
	cell.add_channel('channel_sensory_weak.mc');
};
