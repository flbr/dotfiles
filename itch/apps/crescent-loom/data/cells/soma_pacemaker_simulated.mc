var initialize = func() {
	var cell = registercell();

	cell.name = 'Pacemaker';
	cell.desc = 'Turns on and off.';

	cell.graphic = 'cell_pacemaker.svg';

	cell.capacitance = 3;
	cell.resistance = 1;

	cell.simulate_axons = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_na.mc');

	//set up the options
	cell.add_variants('rhythm', 'fast', 'normal', 'slow');
	cell.set_default_option('rhythm', 'normal');		//will otherwise default to the first one

	cell.add_variants('burst', 'short', 'normal', 'long');
	cell.set_default_option('burst', 'normal');		//will otherwise default to the first one

	//implement the selected options for the current cell:
	var rhythm = cell.get_selected_option('rhythm');
	if (rhythm == 'fast')	{ cell.graphic = 'cell_pacemaker_fast.svg'; cell.add_channel('channel_queer_variable.mc', 'fast'); }
	if (rhythm == 'normal')	{ cell.graphic = 'cell_pacemaker.svg'; cell.add_channel('channel_queer_variable.mc', 'norm'); }
	if (rhythm == 'slow')	{ cell.graphic = 'cell_pacemaker_slow.svg'; cell.add_channel('channel_queer_variable.mc', 'slow'); }

	var burst = cell.get_selected_option('burst');
	if (burst == 'short')	{ cell.add_channel('channel_pacemaker.mc', 'short'); }
	if (burst == 'normal')	{ cell.add_channel('channel_pacemaker.mc', 'norm'); }
	if (burst == 'long')	{ cell.add_channel('channel_pacemaker.mc', 'long'); }

};
