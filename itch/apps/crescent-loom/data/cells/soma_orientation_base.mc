var initialize = func()
{
	var cell = registercell();

	cell.name = 'Orientation';
	cell.desc = 'A handle for an array of cells, which activate when held at certain angles.';

	cell.graphic = 'cell_sensory_orientation.svg';

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_na.mc');

	//not a 'real' cell, just a hollowed-out thing that moves all the others around.
	cell.can_connect = 0;
	cell.can_accept_connections = 0;

	//tell it to make a number of other sensory cells (second number is direction: 0 = north, proceeds clockwise until 5 = north-west)
	cell.add_to_bundle('soma_sensory_graded.mc', 0);
	cell.add_to_bundle('soma_sensory_graded.mc', 1);
	cell.add_to_bundle('soma_sensory_graded.mc', 2);
	cell.add_to_bundle('soma_sensory_graded.mc', 3);
	cell.add_to_bundle('soma_sensory_graded.mc', 4);
	cell.add_to_bundle('soma_sensory_graded.mc', 5);
};

