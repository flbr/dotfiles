var initialize = func()
{
	var cell = registercell();

	cell.name = 'Sensory';
	cell.desc = 'Activated by some kind of external stimuli.';
	cell.graphic = 'cell_sensory_eye.svg';

	cell.capacitance = 2;
	cell.resistance = 1;

	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_big_k.mc');
	cell.add_channel('channel_leak_small_na.mc');

	//three of these so organs can individually turn them on
	cell.add_channel('channel_sensory_weak.mc');
	cell.add_channel('channel_sensory_weak.mc');
	cell.add_channel('channel_sensory_weak.mc');
	cell.add_channel('channel_sensory_weak.mc');

	//set up the options
	cell.add_variants('sees', 'all', 'rock', 'biology');
	cell.set_default_option('objects', 'all');

	cell.add_variants('touchy', 'high', 'normal', 'low');
	cell.set_default_option('touchy', 'normal');

	cell.add_slider('arc', 15, 60, 150);
	cell.set_default_option('arc', 60);

	cell.add_slider('range%', .25, 1.0, 2.0);
	cell.set_default_option('range%', 1.0);
};
