var initialize = func()
{
	var cell = registercell();

	cell.name = 'Basic';
	cell.desc = 'Action potentials in, action potentials out.';

	cell.graphic = 'cell1.svg';

	cell.capacitance = 4;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_k.mc');
	cell.add_channel('channel_leak_small_na.mc');

	cell.add_variants('jitter', 'slight', 'some', 'lots');
	cell.set_default_option('jitter', 'slight');

	var jitter = cell.get_selected_option('jitter');
	cell.add_channel('channel_stochastic_na.mc', jitter);
	cell.add_channel('channel_stochastic_k.mc', jitter);
};
