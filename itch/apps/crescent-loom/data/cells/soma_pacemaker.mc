var initialize = func() {
	var cell = registercell();

	cell.name = 'Pacemaker';
	cell.desc = 'Turns on and off.';

	cell.graphic = 'cell_pacemaker.svg';

	cell.capacitance = 3;
	cell.resistance = 1;

	cell.add_channel('channel_leak_big_k.mc');	//potassium leak channel to set the resting potential
	cell.add_channel('channel_leak_small_na.mc');

	//set up the options
	cell.add_variants('rhythm', 'fast', 'normal', 'slow');
	cell.set_default_option('rhythm', 'normal');		//will otherwise default to the first one

	cell.add_variants('burst', 'short', 'normal', 'long');
	cell.set_default_option('burst', 'normal');		//will otherwise default to the first one

	//implement the selected options for the current cell:
	var rhythm = cell.get_selected_option('rhythm');
	if (rhythm == 'fast')   { cell.add_channel('channel_queer_variable.mc', 'fast'); cell.graphic = 'cell_pacemaker_fast.svg'; }
	if (rhythm == 'normal')	{ cell.add_channel('channel_queer_variable.mc', 'norm'); cell.graphic = 'cell_pacemaker.svg'; }
	if (rhythm == 'slow')	{ cell.add_channel('channel_queer_variable.mc', 'slow'); cell.graphic = 'cell_pacemaker_slow.svg'; }

	var burst = cell.get_selected_option('burst');
	if (burst == 'short')  { cell.add_channel('channel_pacemaker.mc', 'short'); }
	if (burst == 'normal') { cell.add_channel('channel_pacemaker.mc', 'normal'); }
	if (burst == 'long')   { cell.add_channel('channel_pacemaker.mc', 'long'); }

	cell.add_variants('jitter', 'slight', 'some', 'lots');
	cell.set_default_option('jitter', 'slight');
	var jitter = cell.get_selected_option('jitter');
	if (jitter !== 'none') { cell.add_channel('channel_stochastic_k.mc', jitter); }
};
