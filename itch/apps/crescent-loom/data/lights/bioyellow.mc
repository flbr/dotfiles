var initialize = func()
{
	var light = registerlight();

	light.scale = 1.3;
	light.alpha = 0.3;

	light.r = .9;
	light.g = .9;
	light.b = .2;

	light.pulseStrength = .25;
};
