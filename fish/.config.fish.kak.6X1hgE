### variables ###
set TERM "rxvt-unicode"
set fish_greeting
set BROWSER /usr/bin/min
set EDITOR /usr/bin/micro

### startup commands ###
starship init fish | source
wal -R

### aliases ###
alias cls='clear && ls'
alias cdsh='cd ~/Documents/School'
alias cdme='cd ~/Documents/Mineral-Existence'
alias cdh='cd ~/'
alias ..='cd ..'
alias bfg='java -jar ~/bfg.jar'

alias confish='micro ~/.config/fish/config.fish'
alias constar='micro ~/.config/starship.toml'

### symlinks ###

### functions ###
function clean-unzip --argument zipfile
    if not test (echo $zipfile | string sub --start=-4) = .zip
        echo (status function): argument must be a zipfile
        return 1
    end

    if is-clean-zip $zipfile
        unzip $zipfile
    else
        set zipname (echo $zipfile | trim-right '.zip')
        mkdir $zipname || return 1
        unzip $zipfile -d $zipname
    end
end

function file-exists --argument file
    test -e $file
end

function ip-addr
    curl icanhazip.com
end

function random-file
    find . -type f | shuf -n1
end

function fontviewer
    set choice (fc-list | awk '{print $1}' | sed 's/://g' | dmenu -l 20 -p 'Fontviewer: ')
    display "$choice"
end

function gitquick --argument remote branch
    git add .
    git commit -m "quick update"
    git push $remote $branch
end

function gp --argument remote branch
    git push $remote $branch
end

function gacm --argument message
    git add .
    git commit -m $message
end

function imgopt --argument size from
	mogrify \
	-format gif \
	-filter Triangle \
	-define filter:support=2 \
	# a number
	-thumbnail $size \
	-monochrome \
	-unsharp 0.25x0.08+8.3+0.045 \
	-dither FloydSteinberg \
	-colors 2 \
	-posterize 136 \
	-quality 82 \
	-define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all \
	-interlace none \
	-colorspace gray \
	-normalize \
	# a source image
	$from
end

function imgbow --argument size from
	mogrify \
	-format gif \
	# a number
	-thumbnail $size \
	# a source image
	-type Grayscale \
	-threshold 30% \
	$from
end
