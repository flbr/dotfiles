# <a href="home.html">Home</a>

# <a href="now.html">now</a>
# <a href="wiki.html">wiki</a>
# <a href="about.html">about</a>
## Hello there
Thank you for visiting my humble virtual abode. This site is hopefully the final iteration of such a place, and will serve as a space for me to talk about my projects and thoughts, as well as a personal knowledge base in the form of the <a href="wiki.html">wiki</a> page. It is inspired by many sites from the <a href="https://merveilles.town/about">Town of Merveilles</a> and the <a href="https://webring.xxiivv.com/#icons">Webring</a>
Here is what I'm working on <a href="now.html">now</a>.
And this is just a list of every page on this site, ordered by last updated. Eventually I want to implement a slightly more interesting way to navigate pages.
- 290921 - <a href="home.html">Home</a>
- 250921 - <a href="music.html">Music</a>
- 180921 - <a href="software.html">Software</a>
- 180921 - <a href="recipes.html">Recipes</a>
- 180921 - <a href="files.html">Files</a>
- 170921 - <a href="about.html">About</a>
- 170921 - <a href="games.html">Games</a>
- 170921 - <a href="inventory.html">Inventory</a>
- 160921 - <a href="uxn_notes.html">Uxn Notes</a>
- 010921 - <a href="grimoire.html">Grimoire</a>
- 260821 - <a href="tools.html">Tools</a>
- 170821 - <a href="hello.html">Hello!</a>
- 160821 - <a href="wiki.html">Wiki</a>
- 040821 - <a href="now.html">Now</a>
- 010821 - <a href="site.html">Updates</a>
- 010821 - <a href="strawberry_canyon_080421.html">Strawberry Canyon Hike</a>
- 010821 - <a href="sands-of-time.html">The Sands of Time</a>
- 010821 - <a href="side_quests.html">Side Quests</a>
- 010821 - <a href="norwegian.html">Norsk</a>
- 010821 - <a href="plan9.html">Plan 9</a>
- 010821 - <a href="wfc_tile_generator.html">WFC Tile Generator</a>
- 010821 - <a href="wfc_propagator_constructor.html">WFC Propagator Constructor</a>
- 010821 - <a href="writing.html">Writings</a>
- 010821 - <a href="overview_to_wfc.html">Overview of WFC</a>
- 010821 - <a href="intro_to_wfc.html">Intro to WFC</a>
- 010821 - <a href="learning_rust.html">Learning Rust</a>
- 010821 - <a href="marin_headlands_200221.html">Marin Headlands Hike</a>
- 010821 - <a href="drawings.html">Drawings</a>
- 010821 - <a href="hiking.html">Hiking</a>
- 010821 - <a href="devtober.html">Devtober</a>
- 010821 - <a href="backyard_hike_260321.html">Backyard Hike</a>
- 010821 - <a href="bucephalus.html">Bucephalus</a>
- 010821 - <a href="index.html">Index</a>
<a href="mailto:benrhammond@gmail.com" aria-label="Email adress" title="Email adress"></a>
<a rel="me" href="https://merveilles.town/@flbr" aria-label="Fediverse account" title="Fediverse account"></a>
<a rel="me" href="https://github.com/flber" aria-label="Github account" title="Github account"></a>
<a href='https://webring.xxiivv.com/#random' target='_blank'><img class="webring" src='https://webring.xxiivv.com/icon.black.svg' alt="the merveilles webring"/></a>
*Last updated: 290921*
