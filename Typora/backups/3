The aim of this wiki is to build a form of **personal assistant** to help with the management of a vast repository of recorded statistics which includes <a href='journal.html'>daily logs</a>, notes on <a href='index.html'>various projects</a> and <a href='mirrors.html'>curated pages of general knowledge</a>.
<a href='oscean.html'>Oscean</a> is written for a <a href='uxntal.html'>portable virtual machine</a> with of 32 opcodes and 64kb of memory, and maintained from <a href='permacomputing.html'>low-power devices</a>. It is built to adapt to my needs as they change, and to <a href='longtermism.html'>technology as it evolves</a>. The databases are in the <a href='tablatal.html'>tbtl</a>/<a href='indental.html'>ndtl</a> human-readable plaintext formats.
Each part of this project should aim to persist across <a href='longtermism.html'>Technological Long Term</a>, not one part of it should rely on heavy dependencies. — Every function should be **specific**, **unobfuscated**, and each one carefully chosen against general-purpose libraries, frameworks or wasteful foreign entities.
Using this tool should be **frictionless and undisruptive**, its formats and subsequent products versionable, re-purposable, interpretable and text-editable. Only through **open sources, open standards, human-readable formats** and their independencies, might they survive this fleeting age of self-destructing informatics.
These attributes should not only be **perceptible in its design**, <br />but deeply **rooted in its code**.

------

This type of website is a often referred to as a "memex", a kind of archive and mirror of everything that one has done, that one has learnt. It's a living document that outlines where one has been, and a tool that advises where one could go.
> Consider a future device, a sort of mechanized private library in which an individual stores all his books, records, and communications, and which may be consulted with exceeding speed and flexibility. It is an enlarged intimate supplement to his memory.
##### —Vannevar Bush, As We May Think
### License
The license applies to all the **documented projects, the projects themselves and their assets**. The <a href='http://github.com/XXIIVV/Oscean' target='_blank'>platform code</a> is under the **MIT License**. The <a href='https://creativecommons.org/licenses/by-nc-sa/4.0/' target='_blank'>assets and text content</a> is under the **BY-NC-SA4.0 License**.
*You are free to*: **Share**: copy and redistribute the material in any medium or format. **Adapt**: remix, transform, and build upon the material.
*Under the following terms*: **Attribution**: You must give appropriate credit. **NonCommercial**: You may not use the material for commercial purposes. **ShareAlike**: You must distribute your contributions under the same license.
You can learn more about the <a href='oscean.html'>related tools</a> by visiting the <a href='nataniev.html'>nataniev</a> portal, or by reading the <a href='faqs.html'>faqs</a>. You can download a copy of the entire website content and sources as a <a href='https://github.com/XXIIVV/Oscean/archive/master.zip' target='_blank'>.zip</a>.
If you have any **question or feedback**, please submit a <a href='https://github.com/XXIIVV/Oscean/issues/new' target='_blank'>bug report</a>, for any additional informations, contact <a href='devine_lu_linvega.html'>Devine Lu Linvega</a>. 
<img src='../media/services/button.gif'/>
- <a href='https://github.com/XXIIVV/Oscean' target='_blank'>source</a>
- <a href='https://wiki.xxiivv.com/links/rss.xml' target='_blank'>rss feed</a>
- <a href='https://wiki.xxiivv.com/links/tw.txt' target='_blank'>twtxt feed</a>
<img src="../media/diary/075.jpg" alt="Ambigram"/>*Ambigram*<ul><footer><a href="https://creativecommons.org/licenses/by-nc-sa/4.0" target="_blank"><img src="../media/icon/cc.svg" alt="CreativeCommons"/></a><a href="http://webring.xxiivv.com/" target="_blank"><img src="../media/icon/webring.svg" alt="Webring"/></a><a href="https://merveilles.town/@neauoire" target="_blank"><img src="../media/icon/merveilles.svg" alt="Merveilles"/></a><a href="uxn.html"><img src="../media/icon/uxn.svg" alt="Uxn Powered"/></a><a href="devine_lu_linvega.html">Devine Lu Linvega</a> &copy; 2021 &mdash; <a href="about.html">BY-NC-SA 4.0</a> 
