use std::env;
use std::fmt;
use std::fs::{File, OpenOptions};
use std::io::Write;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
const MTRACK_LOG: &'static str = ".config/mtrack/log.txt";

fn get_banner() -> std::string::String {
    format!("{} Version {}
Parses mtrack mood log\n", AUTHORS, VERSION,)
}

fn usage() {
    println!("{}", get_banner());
    println!("{}", "USAGE:
    mparse [mtrack log file]");
}

struct Entry {
	date: String,
	change: String,
	note: String,
}

impl fmt::Display for Entry {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_fmt(format_args!("{} {} {}", self.date, self.change, self.note))
    }
}

trait List<T> {
    fn new(PathBuf) -> Option<T>;
}

type Log = Vec<Entry>;

impl List<Log> for Log {
    fn new(path: PathBuf) -> Option<Log> {
	    let file = File::open(path).expect("[ ERROR ] Failed to open file!");
	    let reader = BufReader::new(file);

	    for line in reader.lines() {
	        let content = line.unwrap();
	        let e = content
	            .parse::<Entry>()
	            .expect("[ ERROR ] failed to parse file!");
	        task_list = diem::apply(e, task_list);
	    }
	    println!("{}", CLIList(task_list));
	    Ok(())
    }

}


fn main() -> std::io::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    match &args[..] {
        [_, path] => {
        	let file_buf = PathBuf::from(path);
        	
        },
        _ => usage(),
    }

    Ok(println!("Done!"))
}

fn todays_list(path: &PathBuf) -> std::io::Result<()> {
    let file = File::open(path).expect("[ ERROR ] Failed to open file!");
    let reader = BufReader::new(file);

    let mut task_list = diem::List::new();

    for line in reader.lines() {
        let content = line.unwrap();
        let e = content
            .parse::<diem::Event>()
            .expect("[ ERROR ] failed to parse file!");
        task_list = diem::apply(e, task_list);
    }
    println!("{}", CLIList(task_list));
    Ok(())
}


fn append(s: &String, path: &PathBuf) -> std::io::Result<()> {
    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(path)
        .expect("[ ERROR ] Failed to open file!");
    file.write_fmt(format_args!("{}\n", s))
}
