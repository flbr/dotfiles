curl http://www.bay12games.com/dwarves/df_47_04_linux.tar.bz2 --output df_47_04_linux.tar.bz2
tar -xjf df_47_04_linux.tar.bz2
rm df_47_04_linux.tar.bz2

cd df_linux

curl -LOk "https://github.com/DFHack/dfhack/releases/download/0.47.04-r3/dfhack-0.47.04-r3-Linux-64bit-gcc-7.tar.bz2" --output dfhack.tar.bz2
tar -xjf dfhack-0.47.04-r3-Linux-64bit-gcc-7.tar.bz2
mv dfhack.init-example dfhack.init


