use std::io::{self, Write};

fn main() {
	let width = match terminal_size() {
		Some(s) => s,
		None => (100, 100),
	}.1 as usize;

	let left_pad = width / 10;
	let bar_width = width / 3;

	let max: usize = 1000000;
	let bar = Bar {
		left_pad,
		bar_width,
		max
	};
	for i in 1..max {
		bar.print(i);
	}
	
}

struct Bar {
	left_pad: usize,
	bar_width: usize,
	max: usize,
}

impl Bar {
	fn print(&self, i: usize) {
		let inverse = 1.0 / (i as f32 / self.max as f32);
		let progress = (self.bar_width as f32 / inverse) as usize;
	
		print!("\r{:#left$}{}% [{:=>mid$}{:->right$}", 
			(100.0/inverse).ceil(),
			" ", ">", "]", 
			left = self.left_pad, 
			mid = progress, 
			right = self.bar_width - progress
		);
		io::stdout().flush().unwrap();
	}
}

fn terminal_size() -> Option<(u16, u16)> {
    use std::process::Command;
    use std::process::Stdio;

    let output = Command::new("stty")
	    .arg("size")
	    .arg("-F")
	    .arg("/dev/stderr")
	    .stderr(Stdio::inherit())
	    .output()
	    .unwrap();
	
    let stdout = String::from_utf8(output.stdout).unwrap();
    if !output.status.success() {
    	return None;
    }
    
    // stdout is "rows cols"
    let mut data = stdout.split_whitespace();
    let rows = u16::from_str_radix(data.next().unwrap(), 10).unwrap();
    let cols = u16::from_str_radix(data.next().unwrap(), 10).unwrap();
    Some((rows, cols))
}
