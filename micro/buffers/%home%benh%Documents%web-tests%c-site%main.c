I��SerializedBuffer�� EventHandler�� Cursor�� ModTime��   8��EventHandler�� 	UndoStack�� 	RedoStack��   '��TEStack�� Top�� Size   *��Element�� Value�� Next��   B��	TextEvent�� C�� 	EventType Deltas�� Time��   Z��Cursor�� Loc�� LastVisualX CurSelection�� OrigSelection�� Num   ��Loc�� X Y   ��[2]buffer.Loc�� ��  ��[]buffer.Delta�� ��  0��Delta�� Text
 Start�� End��   ��Time��   �;���j j  j j  ej l     ؜���.k�\ h h  h h  th j     ؜��6p2n�\ f f  f f  if h     ؜��0�X]�\ d d  d d  sd f     ؜��(F�h�\ b b  b b  -b d     ؜���0�\ ` `  ` `  c` b     ؜���F�\ n n` n ` n  logbook` n     ؜���_�\ \ \  \ \  s\ ^     ؜��(Śa�\ Z Z  Z Z  tZ \     ؜���o��\ X X  X X  sX Z     ؜���Jb�\ V V  V V  eV X     ؜����\ T T  T T  tT V     ؜��0�F�\ R R  R R  -R T     ؜��N�\ P P  P P  bP R     ؜��3����\ N N  N N  eN P     ؜��.���\ L L  L L  wL N     ؜��'�p�\ N NL N L N  CL N     ؜��'��}�\ H H  H H  sH J     ؜��$p��\ F F  F F  tF H     ؜��/���\ D D  D D  nD F     ؜��*��\ B B  B B  eB D     ؜��i��\ @ @  @ @  m@ B     ؜��5�Ϋ�\ > >  > >  u> @     ؜��*EJ��\ < <  < <  c< >     ؜��"�5v�\ : :  : :  o: <     ؜���H��\ 8 8  8 8  D8 :     ؜�����\ H H8 H 8 H  projects8 H     ؜���tH�\ 4 4  4 4  h4 6     ؜��?j�\ 2 2  2 2  n2 4     ؜��;c���\ 0 0  0 0  e0 2     ؜��4�N�\ . .  . .  b. 0     ؜��-A`q�\ > >. > . >  rostiger. >     ؜��-=�a�\     $  
          ���� ��     ؜ަb��\ & & &  $  	standard/ &     ؜ލ��C�\       ��#include "standard/standard.h"
#include <stdio.h>
#include <time.h>

#define STRMEM 4096 * 96
#define LOGMEM 4000
#define LOGPATH "/home/rostiger/projects/C/logbook/logs.md"

typedef struct Block {
  int len;
  char data[STRMEM];
} Block;

typedef struct Time {
  int m, h;
} Time;

typedef struct Log {
  int date;
  Time start, end;
  char *name, *comment;
} Log;

typedef struct Project {
  int len;
  char *name;
  Time total;
  Log entries[LOGMEM];
} Project;

typedef struct Logbook {
  int len, plen;
  Time total;
  Log logs[LOGMEM];
  Project projects[LOGMEM];
} Logbook;

char *push(Block *b, char *s) {
  int i = 0, o = b->len;
  while (s[i])
    b->data[b->len++] = s[i++];
  b->data[b->len++] = '\0';
  return &b->data[o];
}

int error(char *msg, const char *err) {
  printf("Error %s: %s\n", msg, err);
  return 0;
}

int errorid(char *msg, char *val, int id) {
  printf("Error: %s:%d(%s)\n", msg, id, val);
  return 0;
}

Log *makelog(Log *l, char *name) {
  l->date = 0;
  l->name = name;
  return l;
}

Project *makeProject(Project *p, char *name) {
  p->name = name;
  p->len = 0;
  return p;
}

int project_index(char *name, Logbook *logbook) {
  int i;
  if (!&logbook->plen)
    return -1;
  for (i = 0; i < logbook->plen; i++) {
    if (scmp(name, logbook->projects[i].name))
      return i;
  }
  return -1;
}

int ispl(int i, int index, int len) {
  int c = 0;
  char src[128], dst[32];
  sprintf(src, "%d", i);
  dst[0] = '\0';
  while (c < len) {
    dst[c] = src[index + c];
    c++;
  }
  dst[len] = '\0';
  return sint(dst, len + 1);
}

char *stpad(char *src, char *dst, int len, char rune, int right) {
  int srclen, pad, j = 0;
  srclen = slen(src);
  pad = len - srclen;
  dst[0] = '\0';
  while (j <= len) {
    if (!right) {
      if (j < pad)
        dst[j] = rune;
      else
        dst[j] = src[j - pad];
    } else {
      if (j < srclen)
        dst[j] = src[j];
      else
        dst[j] = rune;
    }
    j++;
  }
  dst[j] = '\0';
  return dst;
}

char *ipad(int i, char *dst, int len, char rune, int right) {
  char src[128];
  sprintf(src, "%d", i);
  return stpad(src, dst, len, rune, right);
}

#pragma - mark time

time_t ymdstrtime(int y, int m, int d) {
  struct tm stime;
  stime.tm_year = y - 1900;
  stime.tm_mday = d;
  stime.tm_mon = m - 1;
  stime.tm_hour = 0;
  stime.tm_min = 0;
  stime.tm_sec = 1;
  stime.tm_isdst = -1;
  return mktime(&stime);
}

time_t itotime(int i) {
  int year, month, day;
  year = ispl(i, 0, 2) + 2000;
  month = ispl(i, 2, 2);
  day = ispl(i, 4, 2);
  return ymdstrtime(year, month, day);
}

Time time_diff(Time start, Time end) {
  Time t;
  if (end.m - start.m < 0) {
    t.m = start.m - end.m;
    start.h++;
  } else
    t.m = end.m - start.m;
  t.h = end.h - start.h > 0 ? end.h - start.h : 0;
  return t;
}

Time time_add(Time a, Time b) {
  Time t = a;
  t.m = a.m + b.m;
  t.h = a.h + b.h;
  while (t.m >= 60) {
    t.m -= 60;
    t.h++;
  }
  return t;
}

#pragma - mark entries

void ptotal(Project *p) {
  char name[64], h[16], m[16], e[16];
  stpad(p->name, name, 22, '.', 1);
  ipad(p->total.h, h, 4, ' ', 0);
  ipad(p->total.m, m, 2, '0', 0);
  ipad(p->len, e, 5, ' ', 0);
  printf("%s | %s:%s |%s | %d |\n", name, h, m, e, p->entries[p->len - 1].date);
}

void pentries(Project *p, int start, int end) {
  int i;
  char sh[16], sm[16], eh[16], em[16], h[16], m[16];
  Time dur;
  for (i = 0; i < p->len; i++) {
    Log l = p->entries[i];
    if (l.date < start || l.date > end)
      continue;
    dur = time_diff(l.start, l.end);
    ipad(l.start.h, sh, 2, '0', 0);
    ipad(l.start.m, sm, 2, '0', 0);
    ipad(l.end.h, eh, 2, '0', 0);
    ipad(l.end.m, em, 2, '0', 0);
    ipad(dur.h, h, 2, '0', 0);
    ipad(dur.m, m, 2, '0', 0);
    printf("%d: %s%s - %s%s (%s:%s) - %s\n", l.date, sh, sm, eh, em, h, m,
           l.comment);
  }
}

void pentriestotal(Project *p, int start, int end) {
  int i, entries;
  Time total, dur;
  time_t first, last;
  int days;
  total.h = 0;
  total.m = 0;
  entries = 0;
  for (i = 0; i < p->len; i++) {
    Log l = p->entries[i];
    if (l.date < start || l.date > end)
      continue;
    dur = time_diff(l.start, l.end);
    total = time_add(total, dur);
    entries++;
  }
  first = itotime(start);
  last = itotime(end);
  days = difftime(last, first) / 86400 + 1;
  puts("---------------------------------------------------");
  printf("Project:\t%s\n", p->name);
  printf("Date Range:\t%d - %d (%d days)\n", start, end, days);
  printf("Entries:\t%d logs\n", entries);
  printf("Time:\t\t%d hours", total.h);
  if (total.m > 0)
    printf("and %d minutes\n", total.m);
  else
    puts("\n");
}

#pragma - mark parsing

int parse_logbook(FILE *fp, Block *block, Logbook *logbook) {
  int len, i;
  char *name;
  char line[256], buf1[256], buf2[256];
  Log *l = &logbook->logs[logbook->len];
  Project *p = &logbook->projects[logbook->plen];
  Time diff;
  while (fgets(line, 256, fp)) {
    len = slen(strm(line));
    if (len < 14 || line[0] == ';')
      continue;
    if (logbook->len >= LOGMEM)
      return errorid("Increase memory", "Logbook", logbook->len);
    if (len > 120)
      return errorid("Log is too long", line, len);
    name = strm(sstr(line, buf1, 17, 15));
    l = makelog(&logbook->logs[logbook->len++], push(block, name));
    if (len >= 0)
      l->date = sint(line, 6);
    if (len >= 7) {
      l->start.h = sint(line + 7, 2);
      l->start.m = sint(line + 9, 2);
    }
    if (len >= 12) {
      l->end.h = sint(line + 12, 2);
      l->end.m = sint(line + 14, 2);
    }
    if (len >= 32)
      l->comment = push(block, strm(sstr(line, buf2, 32, 72)));
    i = project_index(name, logbook);
    if (i < 0) {
      p = makeProject(&logbook->projects[logbook->plen++], push(block, name));
    } else
      p = &logbook->projects[i];
    p->entries[p->len] = *l;
    diff = time_diff(l->start, l->end);
    p->total = time_add(p->total, diff);
    logbook->total = time_add(logbook->total, diff);
    p->len++;
  }
  return 1;
}

int parse(Block *block, Logbook *logs) {
  FILE *flogs = fopen(LOGPATH, "r");
  if (!flogs || !parse_logbook(flogs, block, logs)) {
    fclose(flogs);
    return error("Parsing", "Logbook");
  }
  fclose(flogs);
  return 1;
}

Block block;
Logbook logbook;

int main(int argc, char *argv[]) {
  int i, j, start, end;
  Project p;
  if (!parse(&block, &logbook))
    return error("Failure", "Parsing");
  puts("        _                                          ");
  puts("      _//         /        /                       ");
  puts("      /   __ _,  /____ __ /_                       ");
  puts("     /___(_)(_)_/_)(_)(_)/ <_                      ");
  puts("             /|                                    ");
  puts("            |/    v.1.9                            ");
  puts("--------------------------------------------------+");
  if (argc > 1) {
    j = project_index(argv[1], &logbook);
    if (j < 0)
      error("Couldn't find project in logbook", argv[1]);
    else {
      p = logbook.projects[j];
      if (argv[2] && slen(argv[2]) == 6)
        start = sint(argv[2], 6);
      else
        start = p.entries[p.len - 1].date;
      if (argv[3] && slen(argv[3]) == 6)
        end = sint(argv[3], 6);
      else
        end = p.entries[0].date;
      p = logbook.projects[j];
      pentries(&p, start, end);
      pentriestotal(&p, start, end);
    }
  } else {
    puts("Project                 |  hh:mm  |    # |  First |");
    puts("--------------------------------------------------+");
    for (i = 0; i < logbook.plen; i++) {
      p = logbook.projects[i];
      ptotal(&p);
    }
    puts("--------------------------------------------------+");
    printf("Projects......................... %d\n", logbook.plen);
    printf("Entries.......................... %d\n", logbook.len);
    printf("Tracked Time..................... %d:%d\n", logbook.total.h, logbook.total.m);
  }
  return 0;
}
 ��     ؜�_0h ��\       ��#include <stdio.h>

int main() {
  puts("        _                                          ");
  puts("      _//         /        /                       ");
  puts("      /   __ _,  /____ __ /_                       ");
  puts("     /___(_)(_)_/_)(_)(_)/ <_                      ");
  puts("             /|                                    ");
  puts("            |/    v.1.9                            ");
  puts("--------------------------------------------------+");
}
      ؜�_0f�%�\               ؜�^)I0��\       	      ؜�^)GI��\               ؜�^)C���\       	      ؜�^)B t�\               ؜�^)>�#�\       	      ؜�^)<�K�\               ؜�^)9���\       	      ؜�^)7���\         
 
     ؜�^)4K��\       	
 
     ؜�^)2v��\               ؜�^).X(�\       	      ؜�^),z��\               ؜�^)(��\       	      ؜�^)&Ë�\              ؜�^)#Wc�\     
  
      ؜�^)O?�\ x x   
  	      ؜���Hg�\ x x   
  	      ؜���S��\ x x   
  	      ؜���^
�\ x x   
  	      ؜���\�\ x x   
  	      ؜���M+�\ x x   
  	
 
     ؜���
X�\ x x   
  	      ؜����V�\ z z   
         ؜��-��j�\ z z   
         ؜��-���\ z z   
         ؜��-��"�\ z z   
         ؜��-�&7�\ z z   
         ؜��-�<J�\ z z   
   
 
     ؜��-���\ z z   
         ؜��-��4�\ | |   
         ؜����J�\ | |   
         ؜�����\ | |   
         ؜����0�\ | |   
         ؜����\�\ | |   
         ؜����g�\ | |   
   
 
     ؜������\ | |   
         ؜����\     
  
|      ؜ݾp|��\     
  ��  puts("        _                                          ");
  puts("      _//         /        /                       ");
  puts("      /   __ _,  /____ __ /_                       ");
  puts("     /___(_)(_)_/_)(_)(_)/ <_                      ");
  puts("             /|                                    ");
  puts("            |/    v.1.9                            ");
  puts("--------------------------------------------------+");
      ؜ݼ)x�\       	      ؜ݼ
�<�\     
  	      ؜ݻ&�/��\       	      ؜ݺ*l,��\  
     ;      ؜ݺ	����\ 2 8 2    printf("hello, world\n") 2     ؜ݸ7Ri�\ 
 
 
 
 
  \n");
}* 
     ؜��o6�\ 
 
 
 
 
  .c* .     ؜��m�s�\ 
 
 
 
 
  ,        ؜��lP�\ 
 
 
 
 
  intf("      ؜��j��\ 
 
 
 
 
  ograms/      ؜��i@��\       ()
{
	      ؜��e�e�\       g-      ؜��c���\       i      ؜��a��\       ude <stdio.h>

int m
 
     ؜��\�b�\       - 
     ؜��Z�\       #in      ؜��Vդ�\       ../../      ؜��RV��\       #../../c-lang-programs/helloworld.c
      ؜��LS��\                                                                                              �� ��        �� ��     ؜�k4��\    :    ؜�+�Z�\ 