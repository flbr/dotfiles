I��SerializedBuffer�� EventHandler�� Cursor�� ModTime��   8��EventHandler�� 	UndoStack�� 	RedoStack��   '��TEStack�� Top�� Size   *��Element�� Value�� Next��   B��	TextEvent�� C�� 	EventType Deltas�� Time��   Z��Cursor�� Loc�� LastVisualX CurSelection�� OrigSelection�� Num   ��Loc�� X Y   ��[2]buffer.Loc�� ��  ��[]buffer.Delta�� ��  0��Delta�� Text
 Start�� End��   ��Time��   ����      �7local config = require "core.config"
local linter = require "plugins.linter"

config.flake8_args = {}

linter.add_language {
  file_patterns = {"%.py$"},
  warning_pattern = "[^:]:(%d+):(%d+):%s[%w]+%s([^\n]*)",
  command = "flake8 $ARGS $FILENAME",
  args = config.flake8_args,
  expected_exitcodes = {0, 1}
}
      ��'"Q��\         ��'#
���\ 