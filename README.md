# dotfiles

My dotfiles for these programs:

- amfora
- cmus
- cordless
- dmenu-recent
- dunst
- epdfview
- fish
- gitui
- htop
- itch
- Kid3
- kitty
- micro
- mpd
- mpv
- mtrack
- nvim
- obs-studio
- pcmanfm
- qutebrowser
- ranger
- README.md
- sway
- tiny
- twtxt
- Typora
- viewnior
- vlc
- wal
- whatsapp-for-linux
